<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
$gtadb = require(__DIR__ . '/gtadb.php');
$sitedb = require(__DIR__ . '/sitedb.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'gtadb' => $gtadb,
        'sitedb' => $sitedb,
        'gta' => [
        	'class' => 'app\libraries\GtaClient',
        ],
    ],
    'params' => $params,
];
