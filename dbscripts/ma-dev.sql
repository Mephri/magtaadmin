-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: ma-dev
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `backoffice_user`
--

DROP TABLE IF EXISTS `backoffice_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backoffice_user` (
  `admin_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'using sha1',
  `phone_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('active','suspend','remove') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `join_time` datetime NOT NULL,
  `ip_address` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `email` (`email`),
  KEY `login_access` (`email`,`password`,`status`),
  KEY `search_user` (`email`,`fullname`,`phone_number`,`status`,`join_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `var` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `val` text COLLATE utf8_unicode_ci,
  `note` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `code` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gta_hotel_room_category`
--

DROP TABLE IF EXISTS `gta_hotel_room_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gta_hotel_room_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` varchar(60) DEFAULT NULL,
  `hotel_id` int(10) unsigned DEFAULT NULL,
  `room_type_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_hotelid` (`hotel_id`) USING BTREE,
  KEY `idx_roomtypeid` (`room_type_id`) USING BTREE,
  KEY `idx_categoryid` (`category_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gta_hotel_room_type`
--

DROP TABLE IF EXISTS `gta_hotel_room_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gta_hotel_room_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `hotel_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_unique` (`code`,`hotel_id`),
  KEY `idx_hotelid` (`hotel_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel`
--

DROP TABLE IF EXISTS `hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel` (
  `hotel_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `desc_trans_id` bigint(20) unsigned DEFAULT NULL,
  `address` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `area_id` int(11) unsigned NOT NULL,
  `longitude` decimal(23,20) DEFAULT '0.00000000000000000000',
  `latitude` decimal(23,20) DEFAULT '0.00000000000000000000',
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `distance_to_airport` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkin_early` time NOT NULL DEFAULT '13:00:00',
  `checkin_normal` time NOT NULL DEFAULT '14:00:00',
  `checkout_late` time NOT NULL DEFAULT '13:00:00',
  `checkout_normal` time NOT NULL DEFAULT '12:00:00',
  `star_rating` decimal(2,1) unsigned NOT NULL DEFAULT '0.0' COMMENT '1, 2, 3, 4, 5, 6',
  `established_date` date DEFAULT NULL,
  `renovated_date` date DEFAULT NULL,
  `area` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'in metres',
  `public_transportation` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'json format',
  `number_room` smallint(4) DEFAULT NULL,
  `number_floor` smallint(3) DEFAULT NULL,
  `number_restaurant` smallint(3) DEFAULT NULL,
  `voltage` enum('220','110') COLLATE utf8_unicode_ci NOT NULL DEFAULT '220',
  `electric_plug_adapter` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1: UK Universal Grounded, 2: European Universal Grounded, 3: US Non-Grounded',
  `room_service_from` time DEFAULT NULL,
  `room_service_to` time DEFAULT NULL,
  `smoking_room` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hotel_tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source` set('native','gta','hotelbeds','agoda','expedia','asiatravel') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'native',
  `approval_status` enum('approved','review','pending') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `status` enum('enable','disable','removed') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'disable',
  `created_by` mediumint(6) unsigned NOT NULL,
  `approved_by` mediumint(6) unsigned DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `approved_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`hotel_id`),
  KEY `hotel-area_id_fk` (`area_id`),
  KEY `hotel-currency_fk` (`currency`),
  KEY `hotel-user_id_fk` (`created_by`),
  KEY `hotel-group_id_fk` (`group_id`),
  KEY `hotel-trans_id_fk` (`desc_trans_id`),
  CONSTRAINT `hotel-area_id_fk` FOREIGN KEY (`area_id`) REFERENCES `location_area` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `hotel-currency_fk` FOREIGN KEY (`currency`) REFERENCES `currency` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `hotel-group_id_fk` FOREIGN KEY (`group_id`) REFERENCES `hotel_group` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `hotel-trans_id_fk` FOREIGN KEY (`desc_trans_id`) REFERENCES `translation_text` (`translate_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `hotel-user_id_fk` FOREIGN KEY (`created_by`) REFERENCES `backoffice_user` (`admin_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_allotment`
--

DROP TABLE IF EXISTS `hotel_allotment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_allotment` (
  `room_id` bigint(20) unsigned NOT NULL,
  `allotment_date` date NOT NULL,
  `allotment_guaranteed` int(5) NOT NULL DEFAULT '0',
  `allotment_normal` int(5) unsigned NOT NULL DEFAULT '0',
  `allotment_used` int(5) NOT NULL DEFAULT '0',
  `allotment_issued` int(5) NOT NULL DEFAULT '0',
  `close_out` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=normal;1=active',
  `blackout_promo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=normal;1=active',
  `no_arrival` tinyint(1) NOT NULL DEFAULT '0',
  `no_departure` tinyint(1) NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`room_id`,`allotment_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_bank_account`
--

DROP TABLE IF EXISTS `hotel_bank_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_bank_account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20) unsigned NOT NULL,
  `bank_currency` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'IDR',
  `bank_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `bank_branch` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_owner` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `swift_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primary` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `bank_account-hotel_id_fk` (`hotel_id`),
  KEY `bank_account-currency` (`bank_currency`),
  CONSTRAINT `bank_account-currency` FOREIGN KEY (`bank_currency`) REFERENCES `currency` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bank_account-hotel_id_fk` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`hotel_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_cancellation_policy`
--

DROP TABLE IF EXISTS `hotel_cancellation_policy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_cancellation_policy` (
  `policy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20) unsigned NOT NULL,
  `days_before` int(4) DEFAULT '0',
  `percentage` decimal(5,2) DEFAULT '0.00' COMMENT 'Persentase yang dikenakan hotel kepada customer sebagai denda',
  `policy_refund` enum('first_night','all_booking') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`policy_id`),
  UNIQUE KEY `cancellation_id` (`policy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_compulsory_charges`
--

DROP TABLE IF EXISTS `hotel_compulsory_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_compulsory_charges` (
  `room_id` bigint(20) unsigned NOT NULL,
  `charge_date` date NOT NULL,
  `charge_type` enum('people','room') COLLATE utf8_unicode_ci NOT NULL,
  `charge_currency` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'IDR',
  `charge_room` decimal(12,2) DEFAULT '0.00',
  `charge_adult` decimal(12,2) DEFAULT '0.00',
  `charge_child` decimal(12,2) DEFAULT '0.00',
  `comission` decimal(4,2) DEFAULT '0.00',
  `compulsory_type` int(5) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`room_id`,`charge_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_contact`
--

DROP TABLE IF EXISTS `hotel_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_contact` (
  `contact_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20) unsigned NOT NULL,
  `contact_type` enum('sales_marketing','reservation','accounting','other') COLLATE utf8_unicode_ci NOT NULL COMMENT '1: Main, 2: Sales, 3: Allotment, 4: Reservation, 5: Disparity, 6: Account',
  `contact_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_status` enum('enable','disable') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'enable',
  PRIMARY KEY (`contact_id`),
  KEY `contact_type_id_fk` (`contact_type`),
  KEY `contact-hotel_id_fk` (`hotel_id`),
  CONSTRAINT `contact-hotel_id_fk` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`hotel_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_contract`
--

DROP TABLE IF EXISTS `hotel_contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_contract` (
  `contract_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20) unsigned NOT NULL,
  `business_type` enum('regular','deals') COLLATE utf8_unicode_ci DEFAULT 'regular',
  `contract_type` enum('online','offline') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'online' COMMENT 'online=admin hotel yang nginput, offline=bd yang input room and allotment',
  `contract_start_date` date NOT NULL,
  `contract_end_date` date DEFAULT NULL,
  `contract_doc_ref_no` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` mediumint(6) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `created_ip` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`contract_id`),
  KEY `contract_user_id_fk` (`created_by`),
  KEY `contract-hotel_id_fk` (`hotel_id`),
  CONSTRAINT `contract-hotel_id_fk` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`hotel_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `contract_user_id_fk` FOREIGN KEY (`created_by`) REFERENCES `backoffice_user` (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_facility`
--

DROP TABLE IF EXISTS `hotel_facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_facility` (
  `facility_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `facility_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `facility_group` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'General',
  PRIMARY KEY (`facility_id`),
  UNIQUE KEY `facility_name` (`facility_name`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_group`
--

DROP TABLE IF EXISTS `hotel_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_group` (
  `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `group_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `group_name` (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_photo`
--

DROP TABLE IF EXISTS `hotel_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_photo` (
  `photo_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20) unsigned NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo_type_id` tinyint(3) unsigned NOT NULL,
  `primary` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('active','removed') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`photo_id`),
  KEY `search_photo` (`hotel_id`,`status`,`primary`,`filename`),
  KEY `photo-type_id_fk` (`photo_type_id`),
  CONSTRAINT `photo-hotel_id_fk` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`hotel_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `photo-type_id_fk` FOREIGN KEY (`photo_type_id`) REFERENCES `hotel_photo_type` (`photo_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_photo_type`
--

DROP TABLE IF EXISTS `hotel_photo_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_photo_type` (
  `photo_type_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `photo_type_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`photo_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_promotion_type`
--

DROP TABLE IF EXISTS `hotel_promotion_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_promotion_type` (
  `promo_id` int(4) NOT NULL AUTO_INCREMENT,
  `promo_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`promo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_room`
--

DROP TABLE IF EXISTS `hotel_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_room` (
  `room_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20) unsigned NOT NULL,
  `room_type_id` tinyint(3) unsigned NOT NULL,
  `room_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `room_max_occupancy` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `room_max_extrabed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `room_guaranteed_allotment` tinyint(2) NOT NULL DEFAULT '0',
  `room_desc_trans_id` bigint(20) DEFAULT '0',
  `room_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `room_created_by` mediumint(6) unsigned NOT NULL,
  `room_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`room_id`),
  KEY `room-hotel_id_fk` (`hotel_id`),
  KEY `room-type_id_fk` (`room_type_id`),
  CONSTRAINT `room-hotel_id_fk` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`hotel_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `room-type_id_fk` FOREIGN KEY (`room_type_id`) REFERENCES `hotel_room_type` (`room_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_room_facility`
--

DROP TABLE IF EXISTS `hotel_room_facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_room_facility` (
  `room_facility_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `room_facility_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`room_facility_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_room_photo`
--

DROP TABLE IF EXISTS `hotel_room_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_room_photo` (
  `photo_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `room_id` bigint(20) unsigned NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primary` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('active','removed') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`photo_id`),
  KEY `search_photo` (`room_id`,`status`,`primary`,`filename`),
  CONSTRAINT `room_photo-room_id_fk` FOREIGN KEY (`room_id`) REFERENCES `hotel_room` (`room_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_room_rates`
--

DROP TABLE IF EXISTS `hotel_room_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_room_rates` (
  `room_id` int(20) unsigned NOT NULL,
  `rates_date` date NOT NULL,
  `rates_code` enum('RO','B1','B2','BX') COLLATE utf8_unicode_ci NOT NULL,
  `tariff_mode` enum('sell','net','bar') COLLATE utf8_unicode_ci NOT NULL,
  `rates_currency` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'IDR',
  `net_rates` decimal(12,2) NOT NULL DEFAULT '0.00',
  `sell_rates` decimal(12,2) NOT NULL DEFAULT '0.00',
  `extrabed_rates` decimal(12,2) DEFAULT '0.00',
  `comission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `breakfast_person` tinyint(2) NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`room_id`,`rates_date`,`rates_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_room_type`
--

DROP TABLE IF EXISTS `hotel_room_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_room_type` (
  `room_type_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `room_type_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`room_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel_type`
--

DROP TABLE IF EXISTS `hotel_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_type` (
  `type_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `type_name` (`type_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `join_hotel_facility`
--

DROP TABLE IF EXISTS `join_hotel_facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `join_hotel_facility` (
  `hotel_id` bigint(20) unsigned NOT NULL,
  `facility_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`hotel_id`,`facility_id`),
  KEY `join_facility-facility_id_fk` (`facility_id`),
  CONSTRAINT `join_facility-facility_id_fk` FOREIGN KEY (`facility_id`) REFERENCES `hotel_facility` (`facility_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `join_facility-hotel_id_fk` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`hotel_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `join_hotel_type`
--

DROP TABLE IF EXISTS `join_hotel_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `join_hotel_type` (
  `hotel_id` bigint(20) unsigned NOT NULL,
  `type_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`hotel_id`,`type_id`),
  KEY `join_type-type_id_fk` (`type_id`),
  CONSTRAINT `join_type-hotel_id_fk` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`hotel_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `join_type-type_id_fk` FOREIGN KEY (`type_id`) REFERENCES `hotel_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `join_room_facility`
--

DROP TABLE IF EXISTS `join_room_facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `join_room_facility` (
  `room_id` bigint(20) unsigned NOT NULL,
  `room_facility_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`room_id`,`room_facility_id`),
  KEY `join-amenity_id_fk` (`room_facility_id`),
  CONSTRAINT `join-room_facility_id_fk` FOREIGN KEY (`room_facility_id`) REFERENCES `hotel_room_facility` (`room_facility_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `join-room_id_fk` FOREIGN KEY (`room_id`) REFERENCES `hotel_room` (`room_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` double(10,7) NOT NULL,
  `longitude` float(10,7) NOT NULL,
  `altitude` float(5,1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10568 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location_area`
--

DROP TABLE IF EXISTS `location_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_area` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `city_id` mediumint(6) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `area-city_id_fk` (`city_id`),
  CONSTRAINT `area-city_id_fk` FOREIGN KEY (`city_id`) REFERENCES `location_city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75536 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location_city`
--

DROP TABLE IF EXISTS `location_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_city` (
  `id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `state_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `city-state_id_fk` (`state_id`),
  CONSTRAINT `city-state_id_fk` FOREIGN KEY (`state_id`) REFERENCES `location_state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47191 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location_country`
--

DROP TABLE IF EXISTS `location_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_country` (
  `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=764 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location_state`
--

DROP TABLE IF EXISTS `location_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_state` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` smallint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `state-country_id_fk` (`country_id`),
  CONSTRAINT `state-country_id_fk` FOREIGN KEY (`country_id`) REFERENCES `location_country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21588 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `member_account`
--

DROP TABLE IF EXISTS `member_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_account` (
  `account_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'using sha1',
  `gender` enum('M','F') COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secondary_contact` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` char(20) COLLATE utf8_unicode_ci DEFAULT 'ID',
  `source` enum('register','giveaway','facebook','twitter','express','manual') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'register',
  `role` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `newsletter` tinyint(2) NOT NULL DEFAULT '1',
  `authentication` enum('pending','valid') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `status` enum('active','suspend','remove') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `join_time` datetime NOT NULL,
  `modify_time` datetime DEFAULT NULL,
  `ip_address` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `email` (`email`),
  KEY `login_access` (`email`,`password`,`status`),
  KEY `search_user` (`email`,`fullname`,`phone_number`,`status`,`join_time`),
  KEY `demografi_user` (`email`,`join_time`,`gender`,`birth_date`,`status`,`authentication`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `member_profile`
--

DROP TABLE IF EXISTS `member_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_profile` (
  `profile_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) unsigned NOT NULL,
  `salutation` enum('Mr','Mrs','Ms','Miss','Sir','Dr') COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` enum('M','F') COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` date DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secondary_contact` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `identity_card` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_time` datetime NOT NULL,
  `modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`profile_id`),
  KEY `account_id` (`account_id`,`created_time`),
  KEY `search_profile` (`first_name`,`last_name`,`gender`,`birth_date`,`phone_number`,`created_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth_authorization_codes`
--

DROP TABLE IF EXISTS `oauth_authorization_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect_uri` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`authorization_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `client_secret` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `grant_types` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scope` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth_jwt`
--

DROP TABLE IF EXISTS `oauth_jwt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_key` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth_scopes`
--

DROP TABLE IF EXISTS `oauth_scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_scopes` (
  `scope` text COLLATE utf8_unicode_ci,
  `is_default` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth_users`
--

DROP TABLE IF EXISTS `oauth_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_users` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `point_of_interest`
--

DROP TABLE IF EXISTS `point_of_interest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `point_of_interest` (
  `poi_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `latitude` decimal(23,20) NOT NULL DEFAULT '0.00000000000000000000',
  `longitude` decimal(23,20) NOT NULL DEFAULT '0.00000000000000000000',
  `place_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc_trans_id` bigint(20) unsigned DEFAULT NULL,
  `image_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `poi_uri` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` set('airport','amusement_park','art_gallery','bus','other','bus_station','cafe','casino','hindu_temple','hospital','movie_theater','museum','night_club','park','restaurant','shoe_store','shopping_mall','spa','stadium','subway_station','train_station','university','zoo') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'other',
  `status` enum('Online','Offline') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Offline' COMMENT 'Online = will be available in website, after verified by BD, Offline',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`poi_id`),
  UNIQUE KEY `lat_lng` (`latitude`,`longitude`),
  KEY `search_poi` (`latitude`,`longitude`,`place_name`,`category`,`poi_uri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subscriber`
--

DROP TABLE IF EXISTS `subscriber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriber` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_subscribe` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_valid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `register_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subscriber_mailchimp`
--

DROP TABLE IF EXISTS `subscriber_mailchimp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriber_mailchimp` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `register_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `translation_text`
--

DROP TABLE IF EXISTS `translation_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_text` (
  `translate_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `translate_type` enum('general','description','facility','label') COLLATE utf8_unicode_ci DEFAULT 'general',
  `output_id` text COLLATE utf8_unicode_ci,
  `output_en` text COLLATE utf8_unicode_ci,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`translate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `view_location`
--

DROP TABLE IF EXISTS `view_location`;
/*!50001 DROP VIEW IF EXISTS `view_location`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_location` (
  `id` tinyint NOT NULL,
  `area` tinyint NOT NULL,
  `city` tinyint NOT NULL,
  `state` tinyint NOT NULL,
  `country` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_location2`
--

DROP TABLE IF EXISTS `view_location2`;
/*!50001 DROP VIEW IF EXISTS `view_location2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_location2` (
  `country_id` tinyint NOT NULL,
  `country` tinyint NOT NULL,
  `state_id` tinyint NOT NULL,
  `state` tinyint NOT NULL,
  `city_id` tinyint NOT NULL,
  `city` tinyint NOT NULL,
  `area_id` tinyint NOT NULL,
  `area` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `view_location`
--

/*!50001 DROP TABLE IF EXISTS `view_location`*/;
/*!50001 DROP VIEW IF EXISTS `view_location`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_location` AS (select `a`.`id` AS `id`,`a`.`name` AS `area`,`b`.`name` AS `city`,`c`.`name` AS `state`,`d`.`name` AS `country` from (((`location_area` `a` join `location_city` `b` on((`a`.`city_id` = `b`.`id`))) join `location_state` `c` on((`b`.`state_id` = `c`.`id`))) join `location_country` `d` on((`c`.`country_id` = `d`.`id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_location2`
--

/*!50001 DROP TABLE IF EXISTS `view_location2`*/;
/*!50001 DROP VIEW IF EXISTS `view_location2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_location2` AS (select `a`.`id` AS `country_id`,`a`.`name` AS `country`,`b`.`id` AS `state_id`,`b`.`name` AS `state`,`c`.`id` AS `city_id`,`c`.`name` AS `city`,`d`.`id` AS `area_id`,`d`.`name` AS `area` from (((`location_country` `a` left join `location_state` `b` on((`a`.`id` = `b`.`country_id`))) left join `location_city` `c` on((`b`.`id` = `c`.`state_id`))) left join `location_area` `d` on((`c`.`id` = `d`.`city_id`))) order by `a`.`name`,`b`.`name`,`c`.`name`,`d`.`name`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-14  9:58:11
