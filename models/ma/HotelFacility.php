<?php

namespace app\models\ma;

use Yii;

/**
 * This is the model class for table "hotel_facility".
 *
 * @property integer $facility_id
 * @property string $facility_name
 * @property string $facility_group
 *
 * @property JoinHotelFacility[] $joinHotelFacilities
 * @property Hotel[] $hotels
 */
class HotelFacility extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_facility';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sitedb');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['facility_name'], 'required'],
            [['facility_name', 'facility_group'], 'string', 'max' => 64],
            [['facility_name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'facility_id' => 'Facility ID',
            'facility_name' => 'Facility Name',
            'facility_group' => 'Facility Group',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJoinHotelFacilities()
    {
        return $this->hasMany(JoinHotelFacility::className(), ['facility_id' => 'facility_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotels()
    {
        return $this->hasMany(Hotel::className(), ['hotel_id' => 'hotel_id'])->viaTable('join_hotel_facility', ['facility_id' => 'facility_id']);
    }
}
