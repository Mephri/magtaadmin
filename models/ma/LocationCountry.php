<?php

namespace app\models\ma;

use Yii;

/**
 * This is the model class for table "location_country".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 *
 * @property LocationState[] $locationStates
 */
class LocationCountry extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location_country';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sitedb');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['code'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocationStates()
    {
        return $this->hasMany(LocationState::className(), ['country_id' => 'id']);
    }
}
