<?php

namespace app\models\ma;

use Yii;

/**
 * This is the model class for table "hotel_room".
 *
 * @property string $room_id
 * @property string $hotel_id
 * @property integer $room_type_id
 * @property string $room_name
 * @property integer $room_max_occupancy
 * @property integer $room_max_extrabed
 * @property integer $room_guaranteed_allotment
 * @property integer $room_desc_trans_id
 * @property integer $room_active
 * @property integer $room_created_by
 * @property string $room_created_time
 *
 * @property Hotel $hotel
 * @property HotelRoomType $roomType
 * @property HotelRoomPhoto[] $hotelRoomPhotos
 * @property JoinRoomFacility[] $joinRoomFacilities
 * @property HotelRoomFacility[] $roomFacilities
 */
class HotelRoom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_room';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sitedb');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotel_id', 'room_type_id', 'room_name', 'room_created_by'], 'required'],
            [['hotel_id', 'room_type_id', 'room_max_occupancy', 'room_max_extrabed', 'room_guaranteed_allotment', 'room_desc_trans_id', 'room_active', 'room_created_by'], 'integer'],
            [['room_created_time'], 'safe'],
            [['room_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'room_id' => 'Room ID',
            'hotel_id' => 'Hotel ID',
            'room_type_id' => 'Room Type ID',
            'room_name' => 'Room Name',
            'room_max_occupancy' => 'Room Max Occupancy',
            'room_max_extrabed' => 'Room Max Extrabed',
            'room_guaranteed_allotment' => 'Room Guaranteed Allotment',
            'room_desc_trans_id' => 'Room Desc Trans ID',
            'room_active' => 'Room Active',
            'room_created_by' => 'Room Created By',
            'room_created_time' => 'Room Created Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['hotel_id' => 'hotel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomType()
    {
        return $this->hasOne(HotelRoomType::className(), ['room_type_id' => 'room_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelRoomPhotos()
    {
        return $this->hasMany(HotelRoomPhoto::className(), ['room_id' => 'room_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJoinRoomFacilities()
    {
        return $this->hasMany(JoinRoomFacility::className(), ['room_id' => 'room_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomFacilities()
    {
        return $this->hasMany(HotelRoomFacility::className(), ['room_facility_id' => 'room_facility_id'])->viaTable('join_room_facility', ['room_id' => 'room_id']);
    }
}
