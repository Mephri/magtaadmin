<?php

namespace app\models\ma;

use Yii;

/**
 * This is the model class for table "hotel_room_type".
 *
 * @property integer $room_type_id
 * @property string $room_type_name
 *
 * @property HotelRoom[] $hotelRooms
 */
class HotelRoomType extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_room_type';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sitedb');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_type_name'], 'required'],
            [['room_type_name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'room_type_id' => 'Room Type ID',
            'room_type_name' => 'Room Type Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelRooms()
    {
        return $this->hasMany(HotelRoom::className(), ['room_type_id' => 'room_type_id']);
    }
}
