<?php

namespace app\models\ma;

use Yii;

/**
 * This is the model class for table "hotel".
 *
 * @property string $hotel_id
 * @property integer $group_id
 * @property string $name
 * @property string $desc_trans_id
 * @property string $address
 * @property string $zipcode
 * @property integer $area_id
 * @property string $longitude
 * @property string $latitude
 * @property string $phone
 * @property string $fax
 * @property string $website
 * @property string $currency
 * @property string $commission
 * @property string $distance_to_airport
 * @property string $checkin_early
 * @property string $checkin_normal
 * @property string $checkout_late
 * @property string $checkout_normal
 * @property string $star_rating
 * @property string $established_date
 * @property string $renovated_date
 * @property string $area
 * @property string $public_transportation
 * @property integer $number_room
 * @property integer $number_floor
 * @property integer $number_restaurant
 * @property string $voltage
 * @property integer $electric_plug_adapter
 * @property string $room_service_from
 * @property string $room_service_to
 * @property integer $smoking_room
 * @property string $hotel_tag
 * @property string $uri
 * @property string $source
 * @property string $approval_status
 * @property string $status
 * @property integer $created_by
 * @property integer $approved_by
 * @property string $created_time
 * @property string $approved_time
 *
 * @property LocationArea $area0
 * @property Currency $currency0
 * @property HotelGroup $group
 * @property TranslationText $descTrans
 * @property BackofficeUser $createdBy
 * @property HotelBankAccount[] $hotelBankAccounts
 * @property HotelContact[] $hotelContacts
 * @property HotelContract[] $hotelContracts
 * @property HotelPhoto[] $hotelPhotos
 * @property HotelRoom[] $hotelRooms
 * @property JoinHotelFacility[] $joinHotelFacilities
 * @property HotelFacility[] $facilities
 * @property JoinHotelType[] $joinHotelTypes
 * @property HotelType[] $types
 */
class Hotel extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sitedb');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'desc_trans_id', 'area_id', 'number_room', 'number_floor', 'number_restaurant', 'electric_plug_adapter', 'smoking_room', 'created_by', 'approved_by'], 'integer'],
            [['name', 'address', 'zipcode', 'area_id', 'currency', 'created_by'], 'required'],
            [['address', 'voltage', 'source', 'approval_status', 'status'], 'string'],
            [['longitude', 'latitude', 'commission', 'star_rating'], 'number'],
            [['checkin_early', 'checkin_normal', 'checkout_late', 'checkout_normal', 'established_date', 'renovated_date', 'room_service_from', 'room_service_to', 'created_time', 'approved_time'], 'safe'],
            [['name'], 'string', 'max' => 250],
            [['zipcode', 'area'], 'string', 'max' => 10],
            [['phone', 'fax', 'website', 'distance_to_airport', 'hotel_tag', 'uri'], 'string', 'max' => 255],
            [['currency'], 'string', 'max' => 3],
            [['public_transportation'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'hotel_id' => 'Hotel ID',
            'group_id' => 'Group ID',
            'name' => 'Name',
            'desc_trans_id' => 'Desc Trans ID',
            'address' => 'Address',
            'zipcode' => 'Zipcode',
            'area_id' => 'Area ID',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'phone' => 'Phone',
            'fax' => 'Fax',
            'website' => 'Website',
            'currency' => 'Currency',
            'commission' => 'Commission',
            'distance_to_airport' => 'Distance To Airport',
            'checkin_early' => 'Checkin Early',
            'checkin_normal' => 'Checkin Normal',
            'checkout_late' => 'Checkout Late',
            'checkout_normal' => 'Checkout Normal',
            'star_rating' => 'Star Rating',
            'established_date' => 'Established Date',
            'renovated_date' => 'Renovated Date',
            'area' => 'Area',
            'public_transportation' => 'Public Transportation',
            'number_room' => 'Number Room',
            'number_floor' => 'Number Floor',
            'number_restaurant' => 'Number Restaurant',
            'voltage' => 'Voltage',
            'electric_plug_adapter' => 'Electric Plug Adapter',
            'room_service_from' => 'Room Service From',
            'room_service_to' => 'Room Service To',
            'smoking_room' => 'Smoking Room',
            'hotel_tag' => 'Hotel Tag',
            'uri' => 'Uri',
            'source' => 'Source',
            'approval_status' => 'Approval Status',
            'status' => 'Status',
            'created_by' => 'Created By',
            'approved_by' => 'Approved By',
            'created_time' => 'Created Time',
            'approved_time' => 'Approved Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea0()
    {
        return $this->hasOne(LocationArea::className(), ['id' => 'area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency0()
    {
        return $this->hasOne(Currency::className(), ['code' => 'currency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(HotelGroup::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDescTrans()
    {
        return $this->hasOne(TranslationText::className(), ['translate_id' => 'desc_trans_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(BackofficeUser::className(), ['admin_id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelBankAccounts()
    {
        return $this->hasMany(HotelBankAccount::className(), ['hotel_id' => 'hotel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelContacts()
    {
        return $this->hasMany(HotelContact::className(), ['hotel_id' => 'hotel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelContracts()
    {
        return $this->hasMany(HotelContract::className(), ['hotel_id' => 'hotel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelPhotos()
    {
        return $this->hasMany(HotelPhoto::className(), ['hotel_id' => 'hotel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotelRooms()
    {
        return $this->hasMany(HotelRoom::className(), ['hotel_id' => 'hotel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJoinHotelFacilities()
    {
        return $this->hasMany(JoinHotelFacility::className(), ['hotel_id' => 'hotel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacilities()
    {
        return $this->hasMany(HotelFacility::className(), ['facility_id' => 'facility_id'])->viaTable('join_hotel_facility', ['hotel_id' => 'hotel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJoinHotelTypes()
    {
        return $this->hasMany(JoinHotelType::className(), ['hotel_id' => 'hotel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypes()
    {
        return $this->hasMany(HotelType::className(), ['type_id' => 'type_id'])->viaTable('join_hotel_type', ['hotel_id' => 'hotel_id']);
    }
}
