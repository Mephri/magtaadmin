<?php

namespace app\models\ma;

use Yii;

/**
 * This is the model class for table "gta_hotel_room_category".
 *
 * @property integer $id
 * @property string $category_id
 * @property integer $hotel_id
 * @property integer $room_type_id
 */
class GtaHotelRoomCategory extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gta_hotel_room_category';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sitedb');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotel_id', 'room_type_id'], 'integer'],
            [['category_id'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'hotel_id' => 'Hotel ID',
            'room_type_id' => 'Room Type ID',
        ];
    }
}
