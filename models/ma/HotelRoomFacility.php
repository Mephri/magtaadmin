<?php

namespace app\models\ma;

use Yii;

/**
 * This is the model class for table "hotel_room_facility".
 *
 * @property integer $room_facility_id
 * @property string $room_facility_name
 *
 * @property JoinRoomFacility[] $joinRoomFacilities
 * @property HotelRoom[] $rooms
 */
class HotelRoomFacility extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_room_facility';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sitedb');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_facility_name'], 'required'],
            [['room_facility_name'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'room_facility_id' => 'Room Facility ID',
            'room_facility_name' => 'Room Facility Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJoinRoomFacilities()
    {
        return $this->hasMany(JoinRoomFacility::className(), ['room_facility_id' => 'room_facility_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(HotelRoom::className(), ['room_id' => 'room_id'])->viaTable('join_room_facility', ['room_facility_id' => 'room_facility_id']);
    }
}
