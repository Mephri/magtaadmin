<?php

namespace app\models\ma;

use Yii;

/**
 * This is the model class for table "join_room_facility".
 *
 * @property string $room_id
 * @property integer $room_facility_id
 *
 * @property HotelRoomFacility $roomFacility
 * @property HotelRoom $room
 */
class JoinRoomFacility extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'join_room_facility';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sitedb');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_id', 'room_facility_id'], 'required'],
            [['room_id', 'room_facility_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'room_id' => 'Room ID',
            'room_facility_id' => 'Room Facility ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomFacility()
    {
        return $this->hasOne(HotelRoomFacility::className(), ['room_facility_id' => 'room_facility_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(HotelRoom::className(), ['room_id' => 'room_id']);
    }
}
