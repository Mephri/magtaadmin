<?php

namespace app\models\ma;

use Yii;

/**
 * This is the model class for table "gta_hotel_room_type".
 *
 * @property string $id
 * @property string $code
 * @property string $name
 * @property integer $hotel_id
 */
class GtaHotelRoomType extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gta_hotel_room_type';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sitedb');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotel_id'], 'integer'],
            [['code'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 250],
            [['code', 'hotel_id'], 'unique', 'targetAttribute' => ['code', 'hotel_id'], 'message' => 'The combination of Code and Hotel ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'hotel_id' => 'Hotel ID',
        ];
    }
}
