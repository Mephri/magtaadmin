<?php

namespace app\models\ma;

use Yii;

/**
 * This is the model class for table "join_hotel_facility".
 *
 * @property string $hotel_id
 * @property integer $facility_id
 *
 * @property HotelFacility $facility
 * @property Hotel $hotel
 */
class JoinHotelFacility extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'join_hotel_facility';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('sitedb');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotel_id', 'facility_id'], 'required'],
            [['hotel_id', 'facility_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'hotel_id' => 'Hotel ID',
            'facility_id' => 'Facility ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacility()
    {
        return $this->hasOne(HotelFacility::className(), ['facility_id' => 'facility_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['hotel_id' => 'hotel_id']);
    }
}
