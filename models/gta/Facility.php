<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "facility".
 *
 * @property string $Code
 * @property string $Facility
 * @property integer $FacilityID
 * @property integer $FacilityTypeID
 * @property string $FacilityType
 */
class Facility extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'facility';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Code', 'Facility'], 'required'],
            [['FacilityID', 'FacilityTypeID'], 'integer'],
            [['Code'], 'string', 'max' => 3],
            [['Facility'], 'string', 'max' => 120],
            [['FacilityType'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Code' => 'Code',
            'Facility' => 'Facility',
            'FacilityID' => 'Facility ID',
            'FacilityTypeID' => 'Facility Type ID',
            'FacilityType' => 'Facility Type',
        ];
    }
}
