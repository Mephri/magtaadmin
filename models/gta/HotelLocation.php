<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "hotel_location".
 *
 * @property string $HotelCode
 * @property string $LocationCode
 * @property string $LastUpdated
 */
class HotelLocation extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LastUpdated'], 'safe'],
        	[['HotelCode', 'LocationCode'], 'required'],
            [['HotelCode'], 'string', 'max' => 16],
            [['LocationCode'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'HotelCode' => 'Hotel Code',
            'LocationCode' => 'Location Code',
            'LastUpdated' => 'Last Updated',
		];
    }
}
