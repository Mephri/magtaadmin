<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "hotel_linkmap".
 *
 * @property string $ItemCode
 * @property string $MapLink
 * @property string $LastUpdated
 */
class HotelLinkmap extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_linkmap';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LastUpdated'], 'safe'],
        	[['ItemCode'], 'required'],
            [['ItemCode'], 'string', 'max' => 20],
            [['MapLink'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ItemCode' => 'Item Code',
            'MapLink' => 'Map Link',
            'LastUpdated' => 'Last Updated',
		];
    }
}
