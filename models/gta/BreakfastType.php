<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "breakfasttype".
 *
 * @property string $Code
 * @property string $BreakfastTypeName
 */
class BreakfastType extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breakfasttype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Code'], 'required'],
            [['Code'], 'string', 'max' => 5],
            [['BreakfastTypeName'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Code' => 'Code',
            'BreakfastTypeName' => 'Breakfast Type Name',
        ];
    }
}
