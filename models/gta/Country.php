<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property string $Code
 * @property string $CountryName
 */
class Country extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Code', 'CountryName'], 'required'],
            [['Code'], 'string', 'max' => 2],
            [['CountryName'], 'string', 'max' => 80],
            [['Code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Code' => 'Code',
            'CountryName' => 'Country Name',
        ];
    }
}
