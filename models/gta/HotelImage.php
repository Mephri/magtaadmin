<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "hotel_image".
 *
 * @property integer $id
 * @property string $HotelCode
 * @property string $ImageText
 * @property integer $ImageHeight
 * @property integer $ImageWidth
 * @property string $ImageUrl
 * @property string $ImageUrlThumbnail
 * @property string $LastUpdated
 */
class HotelImage extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LastUpdated'], 'safe'],
        	[['ImageHeight', 'ImageWidth'], 'integer'],
            [['HotelCode'], 'string', 'max' => 10],
            [['ImageText'], 'string', 'max' => 60],
            [['ImageUrl', 'ImageUrlThumbnail'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
        	'HotelCode' => 'Hotel Code',
            'ImageText' => 'Image Text',
            'ImageHeight' => 'Image Height',
            'ImageWidth' => 'Image Width',
            'ImageUrl' => 'Image Url',
            'ImageUrlThumbnail' => 'Image Url Thumbnail',
            'LastUpdated' => 'Last Updated',
		];
    }
}
