<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "hotel_areadetail".
 *
 * @property integer $ID
 * @property string $HotelCode
 * @property string $AreaDetail
 * @property string $LastUpdated
 */
class HotelAreaDetail extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_areadetail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['LastUpdated'], 'safe'],
            [['HotelCode'], 'string', 'max' => 10],
            [['AreaDetail'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'HotelCode' => 'Hotel Code',
            'AreaDetail' => 'Area Detail',
            'LastUpdated' => 'Last Updated',
		];
    }
}
