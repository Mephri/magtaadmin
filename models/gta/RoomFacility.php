<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "roomfacility".
 *
 * @property string $Code
 * @property string $RoomFacility
 */
class RoomFacility extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'roomfacility';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Code', 'RoomFacility'], 'required'],
            [['Code'], 'string', 'max' => 5],
            [['RoomFacility'], 'string', 'max' => 220]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Code' => 'Code',
            'RoomFacility' => 'Room Facility',
        ];
    }
}
