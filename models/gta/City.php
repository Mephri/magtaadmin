<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property string $Code
 * @property string $CityName
 * @property string $CountryCode
 * @property string $AreaCode
 */
class City extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Code', 'CityName'], 'required'],
            [['Code', 'AreaCode'], 'string', 'max' => 5],
            [['CityName'], 'string', 'max' => 80],
            [['CountryCode'], 'string', 'max' => 5],
            [['Code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Code' => 'Code',
            'CityName' => 'City Name',
            'CountryCode' => 'Country Code',
            'AreaCode' => 'Area Code',
        ];
    }
}
