<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "hotel_roomtype".
 *
 * @property string $ItemCode
 * @property string $RoomTypeCode
 * @property integer $RoomTypeID
 * @property string $LastUpdated
 */
class HotelRoomType extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_roomtype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LastUpdated'], 'safe'],
        	[['ItemCode', 'RoomTypeCode'], 'required'],
            [['RoomTypeID'], 'integer'],
            [['ItemCode'], 'string', 'max' => 16],
            [['RoomTypeCode'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ItemCode' => 'Item Code',
            'RoomTypeCode' => 'Room Type Code',
            'RoomTypeID' => 'Room Type ID',
            'LastUpdated' => 'Last Updated',
		];
    }
}
