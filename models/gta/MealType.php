<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "mealtype".
 *
 * @property string $Code
 * @property string $MealTypeName
 */
class MealType extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mealtype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Code'], 'required'],
            [['Code'], 'string', 'max' => 5],
            [['MealTypeName'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Code' => 'Code',
            'MealTypeName' => 'Meal Type Name',
        ];
    }
}
