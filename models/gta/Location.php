<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "location".
 *
 * @property string $CityCode
 * @property string $LocationCode
 * @property string $LocationName
 */
class Location extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CityCode', 'LocationCode', 'LocationName'], 'required'],
            [['CityCode'], 'string', 'max' => 5],
            [['LocationCode'], 'string', 'max' => 5],
            [['LocationName'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CityCode' => 'CityCode',
            'LocationCode' => 'LocationCode',
            'LocationName' => 'Location Name',
        ];
    }
}
