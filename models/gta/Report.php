<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "report".
 *
 * @property integer $ReportID
 * @property string $HotelCode
 * @property string $Report
 * @property string $ReportType
 * @property string $LastUpdated
 */
class Report extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LastUpdated'], 'safe'],
        	[['Report', 'ReportType'], 'required'],
            [['HotelCode'], 'string', 'max' => 10],
            [['Report'], 'string', 'max' => 200],
            [['ReportType'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ReportID' => 'Report ID',
            'HotelCode' => 'Hotel Code',
            'Report' => 'Report',
            'ReportType' => 'Report Type',
            'LastUpdated' => 'Last Updated',
		];
    }
}
