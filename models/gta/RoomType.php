<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "roomtype".
 *
 * @property integer $ID
 * @property string $Code
 * @property string $RoomTypeName
 */
class RoomType extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'roomtype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RoomTypeName'], 'required'],
            [['Code'], 'string', 'max' => 4],
            [['RoomTypeName'], 'string', 'max' => 80],
            [['Code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Code' => 'Code',
            'RoomTypeName' => 'Room Type Name',
        ];
    }
}
