<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "area".
 *
 * @property string $Code
 * @property string $AreaName
 * @property string $CountryCode
 */
class Area extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Code', 'AreaName'], 'required'],
            [['Code', 'CountryCode'], 'string', 'max' => 5],
            [['AreaName'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Code' => 'Code',
            'AreaName' => 'Area Name',
            'CountryCode' => 'Country Code',
        ];
    }
}
