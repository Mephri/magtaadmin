<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "remark".
 *
 * @property string $Code
 * @property string $RemarkText
 */
class Remark extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'remark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Code'], 'required'],
            [['Code'], 'string', 'max' => 5],
            [['RemarkText'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Code' => 'Code',
            'RemarkText' => 'Remark Text',
        ];
    }
}
