<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "hotel_facility".
 *
 * @property string $FacilityCode
 * @property string $HotelCode
 * @property string $Facility
 * @property string $LastUpdated
 */
class HotelFacility extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_facility';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LastUpdated'], 'safe'],
        	[['FacilityCode', 'HotelCode'], 'required'],
            [['FacilityCode'], 'string', 'max' => 5],
            [['HotelCode'], 'string', 'max' => 10],
            [['Facility'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'FacilityCode' => 'Facility Code',
            'HotelCode' => 'Hotel Code',
            'Facility' => 'Facility',
            'LastUpdated' => 'Last Updated',
		];
    }
}
