<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "hotel".
 *
 * @property string $Code
 * @property string $ItemCode
 * @property string $ItemName
 * @property string $CityCode
 * @property string $Category
 * @property integer $StarRating
 * @property integer $RoomCount
 * @property string $AddressLine1
 * @property string $AddressLine2
 * @property string $AddressLine3
 * @property string $AddressLine4
 * @property string $Telephone
 * @property string $Fax
 * @property string $EmailAddress
 * @property string $WebSite
 * @property string $Zipcode
 * @property boolean $HasExtraInfo
 * @property boolean $HasMap
 * @property boolean $HasPictures
 * @property boolean $HasImportantInfo
 * @property boolean $Recommended
 * @property string $Latitude
 * @property string $Longitude
 * @property string $Description
 * @property string $LastUpdated
 */
class Hotel extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LastUpdated'], 'safe'],
        	[['Code', 'ItemCode', 'ItemName', 'CityCode', 'AddressLine1'], 'required'],
            [['StarRating', 'RoomCount'], 'integer'],
            [['HasExtraInfo', 'HasMap', 'HasPictures', 'HasImportantInfo', 'Recommended'], 'boolean'],
            [['Code'], 'string', 'max' => 20],
            [['ItemCode', 'Latitude', 'Longitude'], 'string', 'max' => 16],
            [['ItemName', 'Category', 'EmailAddress', 'WebSite'], 'string', 'max' => 120],
            [['CityCode'], 'string', 'max' => 5],
            [['AddressLine1', 'AddressLine2', 'AddressLine3', 'AddressLine4'], 'string', 'max' => 60],
            [['Telephone', 'Fax'], 'string', 'max' => 40],
            [['Zipcode'], 'string', 'max' => 10],
            [['Description'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Code' => 'Code',
            'ItemCode' => 'Item Code',
            'ItemName' => 'Item Name',
            'CityCode' => 'City Code',
            'Category' => 'Category',
            'StarRating' => 'Star Rating',
            'RoomCount' => 'Room Count',
            'AddressLine1' => 'Address Line1',
            'AddressLine2' => 'Address Line2',
            'AddressLine3' => 'Address Line3',
            'AddressLine4' => 'Address Line4',
            'Telephone' => 'Telephone',
            'Fax' => 'Fax',
            'EmailAddress' => 'Email Address',
            'WebSite' => 'Web Site',
            'Zipcode' => 'Zipcode',
            'HasExtraInfo' => 'Has Extra Info',
            'HasMap' => 'Has Map',
            'HasPictures' => 'Has Pictures',
            'HasImportantInfo' => 'Has Important Info',
            'Recommended' => 'Recommended',
            'Latitude' => 'Latitude',
            'Longitude' => 'Longitude',
            'Description' => 'Description',
            'LastUpdated' => 'Last Updated',
		];
    }
}
