<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "hotel_roomcategory".
 *
 * @property integer $ID
 * @property string $HotelCode
 * @property string $RoomCategoryID
 * @property string $RoomDescription
 * @property string $Description
 * @property string $LastUpdated
 */
class HotelRoomCategory extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_roomcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LastUpdated'], 'safe'],
        	[['RoomDescription', 'Description'], 'string'],
            [['HotelCode'], 'string', 'max' => 10],
            [['RoomCategoryID'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
        	'HotelCode' => 'Hotel Code',
            'RoomCategoryID' => 'Room Category ID',
            'RoomDescription' => 'Room Description',
            'Description' => 'Description',
            'LastUpdated' => 'Last Updated',
		];
    }
}
