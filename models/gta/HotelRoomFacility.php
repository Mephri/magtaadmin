<?php

namespace app\models\gta;

use Yii;

/**
 * This is the model class for table "hotel_roomfacility".
 *
 * @property string $HotelCode
 * @property string $RoomFacilityCode
 * @property string $LastUpdated
 */
class HotelRoomFacility extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->gtadb;  
    }
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hotel_roomfacility';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LastUpdated'], 'safe'],
        	[['HotelCode', 'RoomFacilityCode'], 'required'],
            [['HotelCode'], 'string', 'max' => 10],
            [['RoomFacilityCode'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'HotelCode' => 'Hotel Code',
            'RoomFacilityCode' => 'Room Facility Code',
            'LastUpdated' => 'Last Updated',
		];
    }
}
