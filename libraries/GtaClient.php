<?php

namespace app\libraries;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

Class GtaClient extends Component {
	public $URL = 'https://interface.demo.gta-travel.com/rbsrsapi/RequestListenerServlet';
	public $XMLRequest;
	public $XMLResponseRaw;
	public $XPath;
	public $errno;
	public $clientID = '315';
	public $clientEmail = 'XML@GONLA.COM';
	public $clientPassword = 'PASS';
	public $language = 'en';
	public $httpHeader = array(
				"Content-Type: text/xml; charset=UTF-8",
				"Content-Encoding: UTF-8"
			);
	public $requestMode = 'SYNCHRONOUS'; // 'ASYNCHRONOUS';
	public $responseCallbackURL =  'http://your/callback_url'; // 'http://localhost/php/CallbackResponseHandler.php';

	function curlRequest() {
		// Configure headers, etc for request
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->URL);
		curl_setopt($ch, CURLOPT_TIMEOUT, 180);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->XMLRequest);
// 		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
// 		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
// 		curl_setopt($ch, CURLOPT_SSLVERSION, 3);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->httpHeader);

		// Execute request, store response and HTTP response code
		$data = curl_exec($ch);
		$this->errno = curl_getinfo($ch, CURLOPT_POSTFIELDS);

		curl_close($ch);
		return($data);
	}

	function executeRequest($params=null) {
		$rawData = $this->curlRequest();

		if ($rawData != -1) {
			$this->XMLResponseRaw = $rawData;
		}

		return $this->toArray($this->XMLResponseRaw);
	}

	function downloadRequest() {
		$dateTime = date('Y_m_d_H_i_s');
		$saveTo   = "downloaded/itemDownload_$dateTime.zip";
		$filePath = fopen($saveTo, 'w');

		// Configure headers, etc for request
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->URL);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3600);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
//		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->XMLRequest);
		curl_setopt($ch, CURLOPT_FILE, $filePath);
//		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
//		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
//		curl_setopt($ch, CURLOPT_SSLVERSION, 3);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		$httpHeader = array(
				"Content-Type=application/download",
				"Content-Encoding: UTF-8"
		);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);

		// Execute request, store response and HTTP response code
		$data = curl_exec($ch);
		$this->errno = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		fclose($filePath);

		$zip = new \ZipArchive();
		if ($zip->open($saveTo) === TRUE) {
			$zip->extractTo('downloaded/parsexml/');
			$zip->close();
		}

		return($data);
	}

	private function setRequest($requestBody) {
		$requestData = '<?xml version="1.0" encoding="UTF-8" ?>'."\n";
		$requestData .= '<Request>';

		// Create Request Header
		$requestData .= '<Source>';

		// Add Requestor ID data
		$requestData .= '<RequestorID Client="'.$this->clientID.'" EMailAddress="'.$this->clientEmail.'" Password="'.$this->clientPassword.'"/>';

		// Add Requestor Preferences data
		$requestData .= '<RequestorPreferences Language="'.$this->language.'">';
		$requestData .= '<RequestMode>'.$this->requestMode.'</RequestMode>';

		/*** UNCOMMENT IF USING ASYNCHRONOUS Callback Mode ***/
		if ('ASYNCHRONOUS' == $this->requestMode) {
			$requestData .= '<ResponseURL>'.$this->responseCallbackURL.'</ResponseURL>';
		}

		$requestData .= '</RequestorPreferences>';
		$requestData .= '</Source>';

		// Create Request Body
		$requestData .= '<RequestDetails>';
		$requestData .= $requestBody;
		$requestData .= '</RequestDetails>';

		$requestData .= '</Request>';

		$this->XMLRequest = $requestData;
	}

	public function ItemInformationDownloadRequest($params=array()) {
		$incrementalTag = null;
		if (isset($params['yesterday']))
			$incrementalTag = '<IncrementalDownloads></IncrementalDownloads>';

		if (isset($params['fromDate']) && isset($params['toDate']))
			$incrementalTag = '<IncrementalDownloads>
				<FromDate>'.$params['fromDate'].'</FromDate>
				 <ToDate>'.$params['toDate'].'</ToDate>
			</IncrementalDownloads>';
			
		$requestData = '
			<ItemInformationDownloadRequest ItemType="hotel">
				'.$incrementalTag.'
			</ItemInformationDownloadRequest>';
			
		$this->setRequest($requestData);

		$response = $this->downloadRequest();
		return $response;
	}

	public function SearchHotelPriceRequest($params=array()) {
		extract($params);

		$requestData = '<SearchHotelPriceRequest>';

		// Add destination
		$requestData .= '<ItemDestination DestinationType="'.$destinationType.'" DestinationCode="'.$destinationCode.'"/>';

		if (isset($hotelCode))
			$requestData .= '<ItemCode>'.$hotelCode.'</ItemCode>';

		if (isset($itemName))
			$requestData .= '<ItemName>'.$itemName.'</ItemName>';

		#$requestData .= '<ImmediateConfirmationOnly/>';

		// Add period of stay
		$requestData .= '<PeriodOfStay>';
				$requestData .= '<CheckInDate>'.$checkinDate.'</CheckInDate>';
				$requestData .= '<Duration>'.$duration.'</Duration>';
				$requestData .= '</PeriodOfStay>';

				if (isset($includeBreakdown) && $includeBreakdown == true)
					$requestData .= '<IncludePriceBreakdown/>';

				// Add rooms
				$requestData .= '<Rooms>';
				if (is_array($roomCode)) {
					foreach($roomCode as $idx => $room) {
						if ($idx > 3) break;
						$id = (isset($room['categoryId']) ? 'id="'.$room['categoryId'].'"' : null);
						$requestData .= '<Room Code="'.$room['roomCode'].'" '.$id.' NumberOfRooms="'.$numberOfRooms.'"/>';
					}
				} else {
					$id = (isset($categoryId) ? 'id="'.$categoryId.'"' : null);
					$requestData .= '<Room Code="'.$roomCode.'" '.$id.' NumberOfRooms="'.$numberOfRooms.'"/>';
				}
				$requestData .= '</Rooms>';
				$requestData .= '<OrderBy>pricelowtohigh</OrderBy>';
				$requestData .= '</SearchHotelPriceRequest>';

				$this->setRequest($requestData);
				$response = $this->executeRequest($params);

				return $response;
	}

	public function HotelPriceBreakdownRequest($params=array()) {
		extract($params);

		$requestData = '<HotelPriceBreakdownRequest>
			<City>'.$cityCode.'</City>
			<Item>'.$itemCode.'</Item>
			<PeriodOfStay>
				<CheckInDate>'.$checkinDate.'</CheckInDate>
				<Duration>'.$duration.'</Duration>
			</PeriodOfStay>
			<Rooms>
				<Room Code="'.$roomCode.'" Id="'.$roomCategoryID.'" NumberOfCots="'.$numberOfCots.'">
					<ExtraBeds>
						<Age>'.$age.'</Age>
					</ExtraBeds>
				</Room>
			</Rooms>
		</HotelPriceBreakdownRequest>';
		$this->setRequest($requestData);

		$response = $this->executeRequest($params);
		return $response;
	}

	public function SearchChargeConditionsRequest($params=array()) {
		extract($params);

		$requestData = '<SearchChargeConditionsRequest>
			<ChargeConditionsHotel>
				<City>'.$cityCode.'</City>
				<Item>'.$hotelCode.'</Item>
				<PeriodOfStay>
					<CheckInDate>'.$checkinDate.'</CheckInDate>
					<Duration>'.$duration.'</Duration>
				</PeriodOfStay>
				<Rooms>
					<Room Code="'.$roomCode.'" Id="'.$categoryId.'"></Room>
				</Rooms>
			</ChargeConditionsHotel>
		</SearchChargeConditionsRequest>';
		$this->setRequest($requestData);

		$response = $this->executeRequest($params);
		return $response;
	}

	public function AddBookingRequest($params=array()) {
		extract($params);

		$departureDate = $checkinDate; #= date('Y-m-d');
		$checkoutDate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') + $duration, date('Y')));

		$requestData = '<AddBookingRequest>
			<BookingName>'.$bookingName.'</BookingName>
			<BookingReference>'.$bookingReference.'</BookingReference>
			<BookingDepartureDate>'.$departureDate.'</BookingDepartureDate>';
			
		$requestData .= '<PaxNames>';
		foreach($passengers as $idx => $pass) {
			$requestData .= '<PaxName PaxId="'.($idx+1).'"><![CDATA['.$pass.']]></PaxName>';
		}
		$requestData .= '</PaxNames>';
			
		$requestData .= '<BookingItems>
				<BookingItem ItemType="hotel">
					<ItemReference>1</ItemReference>
					<ItemCity Code="'.$cityCode.'" />
					<Item Code="'.$hotelCode.'" />
					<HotelItem>
						<PeriodOfStay>
							<CheckInDate>'.$checkinDate.'</CheckInDate>
							<CheckOutDate>'.$checkoutDate.'</CheckOutDate>
						</PeriodOfStay>
						<HotelRooms>
							<HotelRoom Code="'.$roomCode.'" Id="'.$categoryId.'">
								<PaxIds>';
		foreach($passengers as $idx => $pass) {
			$requestData .= '<PaxId>'.($idx+1).'</PaxId>';
		}
		$requestData .= '</PaxIds>
							</HotelRoom>
						</HotelRooms>
					</HotelItem>
				</BookingItem>
			</BookingItems>
			</AddBookingRequest>';
		$this->setRequest($requestData); #echo '<pre>' . $requestData .'</pre>';

		$response = $this->executeRequest($params);
		return $response;
	}

	public function CancelBookingRequest($params=array()) {
		extract($params);

		$requestData = '';
		$this->setRequest($requestData);

		$response = $this->executeRequest($params);
		return $response;
	}

	public function SearchCountryRequest($params=array()) {
		extract($params);

		$requestData = '<SearchCountryRequest ISO="true">';

		if (isset($country_name))
			$requestData .= '<CountryName><![CDATA['.$country_name.']]></CountryName>';

		if (isset($country_code))
			$requestData .= '<CountryCode><![CDATA['.$country_code.']]></CountryCode>';

		$requestData .= '</SearchCountryRequest>';
		$this->setRequest($requestData);

		$response = $this->executeRequest($params);
		if (isset($response['Response']['Errors']))
			return $response['Response']['Errors'];
		else if (isset($response['Response']['ResponseDetails']['SearchCountryResponse']['CountryDetails']))
			return $response['Response']['ResponseDetails']['SearchCountryResponse']['CountryDetails'];
		else
			return $response;
	}

	public function SearchAreaRequest($params=array()) {
		extract($params);

		$requestData = '<SearchAreaRequest>';

		if (isset($area_name))
			$requestData .= '<AreaName><![CDATA['.$area_name.']]></AreaName>';

		if (isset($area_code))
			$requestData .= '<AreaCode><![CDATA['.$area_code.']]></AreaCode>';

		$requestData .= '</SearchAreaRequest>';
		$this->setRequest($requestData);

		$response = $this->executeRequest($params);
		if (isset($response['Response']['Errors']))
			return $response['Response']['Errors'];
		else if (isset($response['Response']['ResponseDetails']['SearchAreaResponse']['AreaDetails']))
			return $response['Response']['ResponseDetails']['SearchAreaResponse']['AreaDetails'];
		else
			return $response;
	}

	public function SearchCityRequest($params=array()) {
		extract($params);

		$country_code = isset($country_code) ? $country_code : 'ID';
		$requestData = '<SearchCityRequest CountryCode="'.$country_code.'">';

		if (isset($city_name))
			$requestData .= '<CityName><![CDATA['.$city_name.']]></CityName>';

		if (isset($city_code))
			$requestData .= '<CityCode><![CDATA['.$city_code.']]></CityCode>';

		$requestData .= '</SearchCityRequest>';
		$this->setRequest($requestData);

		$response = $this->executeRequest($params);
		if (isset($response['Response']['Errors']))
			return $response['Response']['Errors'];
		else if (isset($response['Response']['ResponseDetails']['SearchCityResponse']['CityDetails']))
			return $response['Response']['ResponseDetails']['SearchCityResponse']['CityDetails'];
		else
			return $response;
	}

	public function SearchCitiesInAreaRequest($params=array()) {
		extract($params);

		$area_code = isset($area_code) ? $area_code : 0;
		$requestData = '<SearchCitiesInAreaRequest AreaCode="'.$area_code.'">';

		if (isset($city_name))
			$requestData .= '<CityName><![CDATA['.$city_name.']]></CityName>';

		if (isset($city_code))
			$requestData .= '<CityCode><![CDATA['.$city_code.']]></CityCode>';

		$requestData .= '</SearchCitiesInAreaRequest>';
		$this->setRequest($requestData);

		$response = $this->executeRequest($params);
		if (isset($response['Response']['Errors']))
			return $response['Response']['Errors'];
		else if (isset($response['Response']['ResponseDetails']['SearchCitiesInAreaResponse']['CityDetails']))
			return $response['Response']['ResponseDetails']['SearchCitiesInAreaResponse']['CityDetails'];
		else
			return $response;
	}

	public function SearchLocationRequest($params=array()) {
		extract($params);

		if (isset($city_code)) {
			$requestData = '<SearchLocationRequest CityCode="'.$city_code.'">';
		}

		if (isset($location_name))
			$requestData .= '<LocationName><![CDATA['.$location_name.']]></LocationName>';

		if (isset($location_code))
			$requestData .= '<LocationCode><![CDATA['.$location_code.']]></LocationCode>';

		$requestData .= '</SearchLocationRequest>';
		$this->setRequest($requestData);

		$response = $this->executeRequest($params);
		if (isset($response['Response']['Errors']))
			return $response['Response']['Errors'];
		else if (isset($response['Response']['ResponseDetails']['SearchLocationResponse']['LocationDetails']))
			return $response['Response']['ResponseDetails']['SearchLocationResponse']['LocationDetails'];
		else
			return $response;
	}

	public function SearchRoomTypeRequest($params=array()) {
		extract($params);

		$requestData = '<SearchRoomTypeRequest>';

		if (isset($room_type_name))
			$requestData .= '<RoomTypeName><![CDATA['.$room_type_name.']]></RoomTypeName>';

		if (isset($room_type_code))
			$requestData .= '<RoomTypeCode><![CDATA['.$room_type_code.']]></RoomTypeCode>';

		$requestData .= '</SearchRoomTypeRequest>';
		$this->setRequest($requestData);

		$response = $this->executeRequest($params);
		if (isset($response['Response']['Errors']))
			return $response['Response']['Errors'];
		else if (isset($response['Response']['ResponseDetails']['SearchRoomTypeResponse']['RoomTypeDetails']))
			return $response['Response']['ResponseDetails']['SearchRoomTypeResponse']['RoomTypeDetails'];
		else
			return $response;
	}

	public function SearchMealTypeRequest($params=array()) {
		extract($params);

		$requestData = '<SearchMealTypeRequest>';

		if (isset($meal_type_name))
			$requestData .= '<MealTypeName><![CDATA['.$meal_type_name.']]></MealTypeName>';

		if (isset($meal_type_code))
			$requestData .= '<MealTypeCode><![CDATA['.$meal_type_code.']]></MealTypeCode>';

		$requestData .= '</SearchMealTypeRequest>';
		$this->setRequest($requestData);

		$response = $this->executeRequest($params);
		if (isset($response['Response']['Errors']))
			return $response['Response']['Errors'];
		else if (isset($response['Response']['ResponseDetails']['SearchMealTypeResponse']['MealTypeDetails']))
			return $response['Response']['ResponseDetails']['SearchMealTypeResponse']['MealTypeDetails'];
		else
			return $response;
	}

	public function SearchBreakfastTypeRequest($params=array()) {
		extract($params);

		$requestData = '<SearchBreakfastTypeRequest>';

		if (isset($breakfast_type_name))
			$requestData .= '<BreakfastTypeName><![CDATA['.$breakfast_type_name.']]></BreakfastTypeName>';

		if (isset($breakfast_type_code))
			$requestData .= '<BreakfastTypeCode><![CDATA['.$breakfast_type_code.']]></BreakfastTypeCode>';

		$requestData .= '</SearchBreakfastTypeRequest>';
		$this->setRequest($requestData);

		$response = $this->executeRequest($params);
		if (isset($response['Response']['Errors']))
			return $response['Response']['Errors'];
		else if (isset($response['Response']['ResponseDetails']['SearchBreakfastTypeResponse']['BreakfastTypeDetails']))
			return $response['Response']['ResponseDetails']['SearchBreakfastTypeResponse']['BreakfastTypeDetails'];
		else
			return $response;
	}

	public function SearchRemarkRequest($params=array()) {
		extract($params);

		if (isset($item_type)) {
			$requestData = '<SearchRemarkRequest ItemType="'.$item_type.'">';
		} else {
			$requestData = '<SearchRemarkRequest>';
		}

		if (isset($remark_name))
			$requestData .= '<RemarkName><![CDATA['.$remark_name.']]></RemarkName>';

		if (isset($remark_code))
			$requestData .= '<RemarkCode><![CDATA['.$remark_code.']]></RemarkCode>';

		$requestData .= '</SearchRemarkRequest>';
		$this->setRequest($requestData);

		$response = $this->executeRequest($params);
		if (isset($response['Response']['Errors']))
			return $response['Response']['Errors'];
		else if (isset($response['Response']['ResponseDetails']['SearchRemarkResponse']['RemarkDetails']))
			return $response['Response']['ResponseDetails']['SearchRemarkResponse']['RemarkDetails'];
		else
			return $response;
	}

	function toArray($contents, $getAttributes=1, $priority = 'attribute') {
		if(!$contents) return array();

		if(!function_exists('xml_parser_create')) {
			//print "'xml_parser_create()' function not found!";
			return array();
		}

		//Get the XML parser of PHP - PHP must have this module for the parser to work
		$parser = xml_parser_create('');
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($contents), $xml_values);
		xml_parser_free($parser);

		if(!$xml_values) return;//Hmm...

		//Initializations
		$xml_array = array();
		$parents = array();
		$opened_tags = array();
		$arr = array();

		$current = &$xml_array; //Refference

		//Go through the tags.
		$repeated_tag_index = array();//Multiple tags with same name will be turned into an array
		foreach($xml_values as $data) {
			unset($attributes,$value);//Remove existing values, or there will be trouble

			//This command will extract these variables into the foreach scope
			// tag(string), type(string), level(int), attributes(array).
			extract($data);//We could use the array by itself, but this cooler.

			$result = array();
			$attributes_data = array();
			 
			if(isset($value)) {
				if($priority == 'tag') $result = $value;
				else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
			}

			//Set the attributes too.
			if(isset($attributes) and $getAttributes) {
				foreach($attributes as $attr => $val) {
					if($priority == 'tag') $attributes_data[$attr] = $val;
					else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
				}
			}

			//See tag status and do the needed.
			if($type == "open") {//The starting of the tag '<tag>'
				$parent[$level-1] = &$current;
				if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
					$current[$tag] = $result;
					if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
					$repeated_tag_index[$tag.'_'.$level] = 1;

					$current = &$current[$tag];

				} else { //There was another element with the same tag name

					if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						$repeated_tag_index[$tag.'_'.$level]++;
					} else {//This section will make the value an array if multiple tags with the same name appear together
						$current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
						$repeated_tag_index[$tag.'_'.$level] = 2;
						 
						if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
							$current[$tag]['0_attr'] = $current[$tag.'_attr'];
							unset($current[$tag.'_attr']);
						}

					}
					$last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
					$current = &$current[$tag][$last_item_index];
				}

			} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
				//See if the key is already taken.
				if(!isset($current[$tag])) { //New Key
					$current[$tag] = $result;
					$repeated_tag_index[$tag.'_'.$level] = 1;
					if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;

				} else { //If taken, put all things inside a list(array)
					if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

						// ...push the new element into that array.
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						 
						if($priority == 'tag' and $getAttributes and $attributes_data) {
							$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
						}
						$repeated_tag_index[$tag.'_'.$level]++;

					} else { //If it is not an array...
						$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
						$repeated_tag_index[$tag.'_'.$level] = 1;
						if($priority == 'tag' and $getAttributes) {
							if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
								 
								$current[$tag]['0_attr'] = $current[$tag.'_attr'];
								unset($current[$tag.'_attr']);
							}
							 
							if($attributes_data) {
								$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
							}
						}
						$repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
					}
				}

			} elseif($type == 'close') { //End of tag '</tag>'
				$current = &$parent[$level-1];
			}
		}
	  
		return($xml_array);
	}
}
