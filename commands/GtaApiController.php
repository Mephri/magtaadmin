<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\db\Migration;
use app\models\gta\Area;
use app\models\gta\BreakfastType;
use app\models\gta\City;
use app\models\gta\Country;
use app\models\gta\Facility;
use app\models\gta\Hotel;
use app\models\gta\HotelAreaDetail;
use app\models\gta\HotelFacility;
use app\models\gta\HotelImage;
use app\models\gta\HotelLinkmap;
use app\models\gta\HotelLocation;
use app\models\gta\HotelRoomCategory;
use app\models\gta\HotelRoomFacility;
use app\models\gta\HotelRoomType;
use app\models\gta\Location;
use app\models\gta\MealType;
use app\models\gta\Remark;
use app\models\gta\Report;
use app\models\gta\RoomFacility;
use app\models\gta\RoomType;
use app\models\ma\LocationCountry;
use app\models\ma\LocationState;
use app\models\ma\LocationCity;
use app\models\ma\LocationArea;
use app\models\ma\HotelFacility as MAHotelFacility;
use app\models\ma\HotelRoomFacility as MAHotelRoomFacility;

class GtaApiController extends Controller
{
    public function actionIndex()
    {
        echo 'Nothing\'s here.';
    }

    public function actionGetMasterStaticData()
    {
    	echo "Clear data...\r\n\n";
    	Country::deleteAll();
    	Area::deleteAll();
    	City::deleteAll();
    	Location::deleteAll();
    	RoomType::deleteAll();
    	MealType::deleteAll();
    	BreakfastType::deleteAll();
    	Remark::deleteAll();

		echo "Fetch Country...\r\n";
		$countries = Yii::$app->gta->SearchCountryRequest(); 
		
		foreach ($countries['Country'] as $country) {
			$countryCode = $country['attr']['Code'];
			$countryName = $country['value'];
			
			// add country (code, countryName)
			if (Country::findOne($countryCode) == null) {
    			$countryModel = new Country();
				$countryModel->Code = $countryCode;
				$countryModel->CountryName = $countryName;
				$countryModel->insert();
				echo "Add Country: {$countryCode} - {$countryName}\r\n";				
			} else {
				continue;
			}

			echo "Fetch City...\r\n";
			$cities = Yii::$app->gta->SearchCityRequest(array('country_code' => $countryCode));
			if (isset($cities['City'])) {
				if (isset($cities['City']['value'])) {
					$cityCode = $cities['City']['attr']['Code'];
					$cityName = $cities['City']['value'];
					
					// add city (code, cityName, countryCode)
					if (City::findOne($cityCode) == null) {
						$cityModel = new City();
						$cityModel->Code = $cityCode;
						$cityModel->CityName = $cityName;
						$cityModel->CountryCode = $countryCode;
						$cityModel->insert();
						echo "Add City: {$countryCode}, {$cityCode} - {$cityName}\r\n";
					}	
				} else {
					foreach($cities['City'] as $city) {
						$cityCode = $city['attr']['Code'];
						$cityName = $city['value'];

						// add city (code, cityName, countryCode)
						if (City::findOne($cityCode) == null) {
							$cityModel = new City();
							$cityModel->Code = $cityCode;
							$cityModel->CityName = $cityName;
							$cityModel->CountryCode = $countryCode;
							$cityModel->insert();
							echo "Add City: {$countryCode}, {$cityCode} - {$cityName}\r\n";
						}
// 						$locations = Yii::$app->gta->SearchLocationRequest(array('city_code' => $cityCode));
// 						if (isset($locations['Location'])) {
// 							if (isset($locations['Location']['value'])) {
// 								$locationCode = $locations['Location']['attr']['Code'];
// 								$locationName = $locations['Location']['value'];

//								$locationModel = new Location();
// 								$locationModel->CityCode = $cityCode;
// 								$locationModel->LocationCode = $locationCode;
// 								$locationModel->LocationName = $locationName;
// 								$locationModel->insert();
// 								echo "Add Location: {$locationCode}, {$cityCode} - {$cityName}\r\n";
// 							}
// 						} else {
// 							foreach($locations['Location'] as $location) {
// 								$locationCode = $location['attr']['Code'];
// 								$locationName = $location['value'];
								
//								$locationModel = new Location();
// 								$locationModel->CityCode = $cityCode;
// 								$locationModel->LocationCode = $locationCode;
// 								$locationModel->LocationName = $locationName;
// 								$locationModel->insert();
// 								echo "Add Location: {$locationCode}, {$cityCode} - {$cityName}\r\n";
// 							}
// 						}
					}
				}
			}
			
			echo "Fetch Area...\r\n";
			$areas = Yii::$app->gta->SearchAreaRequest(array('area_name' => $countryName));
			if (isset($areas['Area'])) {
				if (isset($areas['Area']['value'])) {
					$areas['Area'][0] = $areas['Area'];
					unset($areas['Area']['attr']);
					unset($areas['Area']['value']);
				}
				foreach($areas['Area'] as $area) { 
					$areaCode = $area['attr']['Code'];
					$areaName = $area['value'];
					
					// add area (code, areaName)
					if (Area::findOne($areaCode) == null) {
						$areaModel = new Area();
						$areaModel->Code = $areaCode;
						$areaModel->AreaName = $areaName;
						$areaModel->CountryCode = $countryCode;
						$areaModel->insert();
						echo "Add Area: {$areaCode} - {$areaName}\r\n";				
					} else {
						continue;
					}
					echo "Fetch CitiesInArea...\r\n";
					$cities = Yii::$app->gta->SearchCitiesInAreaRequest(array('area_code' => $areaCode));
					if (isset($cities['City'])) {
						if (isset($cities['City']['value'])) {
							$cityCode = $cities['City']['attr']['Code'];
							
							// update city(areaCode)
							$cityModel1 = City::findOne($cityCode);
							if ($cityModel1) {
								$cityModel1->AreaCode = $areaCode;
								$cityModel1->update();
								echo "Update City: {$cityCode}, {$areaCode}\r\n";
							}	
						} else {
							foreach($cities['City'] as $city) {
								$cityCode = $city['attr']['Code'];
								$cityName = $city['value'];
								
								// update city(areaCode)
								$cityModel1 = City::findOne($cityCode);
								if ($cityModel1) {
									$cityModel1->AreaCode = $areaCode;
									$cityModel1->update();
									echo "Update City: {$cityCode}, {$areaCode}\r\n";
								}				
							}
						}
					}
				}	
			}
		}
		
		echo "Fetch Room Type...\r\n";
		$roomTypes = Yii::$app->gta->SearchRoomTypeRequest();

		foreach ($roomTypes['RoomType'] as $roomType) {
			$roomTypeCode = $roomType['attr']['Code'];
			$roomTypeName = $roomType['value'];
				
    		$roomTypeModel = new RoomType();
			$roomTypeModel->Code = $roomTypeCode;
			$roomTypeModel->RoomTypeName = $roomTypeName;
			$roomTypeModel->insert();
			echo "Add Room Type: {$roomTypeCode} - {$roomTypeName}\r\n";
		}

		echo "Fetch Meal Type...\r\n";
		$mealTypes = Yii::$app->gta->SearchMealTypeRequest();
		
		foreach ($mealTypes['MealType'] as $mealType) {
			$mealTypeCode = $mealType['attr']['Code'];
			$mealTypeName = $mealType['value'];
		
    		$mealTypeModel = new MealType();
			$mealTypeModel->Code = $mealTypeCode;
			$mealTypeModel->MealTypeName = $mealTypeName;
			$mealTypeModel->insert();
			echo "Add Meal Type: {$mealTypeCode} - {$mealTypeName}\r\n";
		}

		echo "Fetch Breakfast Type...\r\n";
		$breakfastTypes = Yii::$app->gta->SearchBreakfastTypeRequest();
		
		foreach ($breakfastTypes['BreakfastType'] as $breakfastType) {
			$breakfastTypeCode = $breakfastType['attr']['Code'];
			$breakfastTypeName = $breakfastType['value'];
		
    		$breakfastTypeModel = new BreakfastType();
			$breakfastTypeModel->Code = $breakfastTypeCode;
			$breakfastTypeModel->BreakfastTypeName = $breakfastTypeName;
			$breakfastTypeModel->insert();
			echo "Add Breakfast Type: {$breakfastTypeCode} - {$breakfastTypeName}\r\n";
		}	

		echo "Fetch Remark...\r\n";
		$remarks = Yii::$app->gta->SearchRemarkRequest(array('item_type' => 'hotel'));
		
		foreach ($remarks['Remark'] as $remark) {
			$remarkCode = $remark['attr']['Code'];
			$remarkText = $remark['value'];
		
    		$remarkModel = new Remark();
			$remarkModel->Code = $remarkCode;
			$remarkModel->RemarkText = $remarkText;
			$remarkModel->insert();
			echo "Add Remark: {$remarkCode} - {$remarkText}\r\n";
		}
    }

    public function actionSyncMasterStaticData() {
    	$countries = Country::find()->all();
    	foreach ($countries as $country) {
    		echo "\r\n------------------------------------------\r\n";
    		$locationCountryModel = new LocationCountry();
    		$locationCountryModel->code = $country->Code;
    		$locationCountryModel->name = $country->CountryName;
    		if (($foundOne = LocationCountry::findOne(['code' => $country->Code])) == null) {
    			$locationCountryModel->insert();
    			echo "Insert location_country: {$locationCountryModel->code} - {$locationCountryModel->name}\r\n";
    		} else {
    			$locationCountryModel->id = $foundOne->id;
    			$locationCountryModel->update();
    			echo "Update location_country: {$locationCountryModel->code} - {$locationCountryModel->name}\r\n";
    		}
    		
    		echo "\r\n------------------------------------------\r\n";
    		$states = Area::find()->where(['CountryCode' => $country->Code])->all();
    		foreach ($states as $state) {
    			$locationStateModel = new LocationState();
    			$locationStateModel->name = $state->AreaName;
    			$locationStateModel->country_id = $locationCountryModel->id;
    			if (($foundOne = LocationState::findOne(['country_id' => $locationCountryModel->id, 'name' => $state->AreaName])) == null) {
    				$locationStateModel->insert();
    				echo "Insert location_state: {$locationStateModel->name} - {$locationStateModel->country_id}\r\n";
    			} else {
    				$locationStateModel->id = $foundOne->id;
    				$locationStateModel->update();
    				echo "Update location_state: {$locationStateModel->name} - {$locationStateModel->country_id}\r\n";
    			}
    		}
    		
    		echo "\r\n------------------------------------------\r\n";
    		$cities = City::find()->where(['CountryCode' => $country->Code])->all();
    		foreach ($cities as $city) {
    			$locationCityModel = new LocationCity();
    			$locationCityModel->name = $city->CityName;
    			if (($foundOne = Area::findOne(['CountryCode' => $country->Code, 'Code' => $city->AreaCode])) == null) {
    				$areaName = 'Unspecified';
    				if (LocationState::findOne(['country_id' => $locationCountryModel->id, 'name' => 'Unspecified']) == null) {
	    				$locationStateModel = new LocationState();
	    				$locationStateModel->name = $areaName;
	    				$locationStateModel->country_id = $locationCountryModel->id;
	    				$locationStateModel->insert();
	    				echo "Insert location_state Unspecified: {$locationStateModel->name} - {$locationStateModel->country_id}\r\n";
    				}
    			} else {
    				$areaName = $foundOne->AreaName;
    			}
    			$locationCityModel->state_id = LocationState::findOne(['country_id' => $locationCountryModel->id, 'name' => $areaName])->id;
    			if (($foundOne = LocationCity::findOne(['state_id' => $locationCityModel->state_id, 'name' => $city->CityName])) == null) {
    				$locationCityModel->insert();
    				echo "Insert location_city: {$locationCityModel->name} - {$locationCityModel->state_id}\r\n";
    			} else {
    				$locationCityModel->id = $foundOne->id;
    				$locationCityModel->update();
    				echo "Update location_city: {$locationCityModel->name} - {$locationCityModel->state_id}\r\n";
    			}
    			
    			echo "\r\n------------------------------------------\r\n";
    			$locations = Location::find()->where(['CityCode' => $city->Code])->all();
    			foreach ($locations as $location) {
    				$locationAreaModel = new LocationArea();
    				$locationAreaModel->name = $location->LocationName;
    				$locationAreaModel->city_id = $locationCityModel->id;
    				if (($foundOne = LocationArea::findOne(['city_id' => $locationCityModel->id, 'name' => $locationAreaModel->name])) == null) {
    					$locationAreaModel->insert();
    					echo "Insert location_area: {$locationAreaModel->name} - {$locationAreaModel->city_id}\r\n";
    				} else {
    					$locationAreaModel->id = $foundOne->id;
    					$locationAreaModel->update();
    					echo "Update location_area: {$locationAreaModel->name} - {$locationAreaModel->city_id}\r\n";
    				}
    			}
    		}
    	}
    	
    	echo "\r\n------------------------------------------\r\n";
    	$facilities = Facility::find()->all();
    	foreach ($facilities as $facility) {
    		$hotelFacilityModel = new MAHotelFacility();
    		$hotelFacilityModel->facility_name = $facility->Facility;
    		if (($foundOne = MAHotelFacility::findOne(['facility_name' => $facility->Facility])) == null) {
    			$hotelFacilityModel->insert();
    			echo "Insert hotel_facility: {$hotelFacilityModel->facility_name}\r\n";
    		} else {
    			$hotelFacilityModel->facility_id = $foundOne->facility_id;
    			$hotelFacilityModel->update();
    			echo "Update hotel_facility: {$hotelFacilityModel->facility_name}\r\n";
    		}
    	}
    	
    	echo "\r\n------------------------------------------\r\n";
    	$roomFacilities = RoomFacility::find()->all();
    	foreach ($roomFacilities as $roomFacility) {
    		$hotelRoomFacilityModel = new MAHotelRoomFacility();
    		$hotelRoomFacilityModel->room_facility_name = $roomFacility->RoomFacility;
    		if (($foundOne = MAHotelRoomFacility::findOne($hotelRoomFacilityModel->room_facility_name)) == null) {
    			$hotelRoomFacilityModel->insert();
    			echo "Insert hotel_room_facility: {$hotelRoomFacilityModel->room_facility_name}\r\n";
    		} else {
    			$hotelRoomFacilityModel->room_facility_id = $foundOne->room_facility_id; 
    			$hotelRoomFacilityModel->update();
    			echo "Update hotel_room_facility: {$hotelRoomFacilityModel->room_facility_name}\r\n";
    		}
    	}
    }
    
    public function actionGetHotelStaticData($downloadType = null, $interval = null)
    {
//     	$params = array();
//     	if ($downloadType == 'yesterday') {	// download yesterday update only
//     		$params['yesterday'] = true;
//     	} else if ($downloadType == 'interval' && !empty($interval)) {	// download from start date to end date
//     		list($fromDate, $toDate) = explode(',', $interval);
//     		$pattern = '/^[0-9]{4}-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])$/';
//     		if (preg_match($pattern, $fromDate) && preg_match($pattern, $toDate)) {
//     			$params['fromDate'] = $fromDate;
//     			$params['toDate'] = $toDate;
//     		}
//     	}

//     	// remove all file in temporary
//     	if (is_dir('downloaded/parsexml')) {
//     		$this->deleteDir('downloaded/parsexml');
//     	}

//     	// fetch xml.zip from GTA, and extract to temporary file (downloaded/parsexml)
//     	$xml = Yii::$app->gta->ItemInformationDownloadRequest($params);

    	$zip = new \ZipArchive();
		if ($zip->open("downloaded/itemDownload_2015_03_31_10_53_41.zip") === TRUE) {
			$zip->extractTo('downloaded/parsexml/');
			$zip->close();
		}
    	
    	// parse xml and insert into temporary database
    	$files 	= glob("downloaded/parsexml/*.xml");
    	echo "Found ".count($files) ." files\r\n";
    		
    	if (!empty($files)) {
    		foreach($files as $idx => $file) {
    			echo "file: {$idx} => {$file}\r\n";
    			$this->parseXML($file);
    		}
    	}
    	echo "done..\r\n";
    }

	private function parseXML($xmlfile) {
		echo $xmlfile . "\r\n";
		$xml = Yii::$app->gta->toArray(file_get_contents($xmlfile));
		
		$items = $xml['Response']['ResponseDetails']['SearchItemInformationResponse']['ItemDetails']['ItemDetail'];
		
		$itemCode = $items['Item']['attr']['Code'];
		$itemName = addslashes($items['Item']['value']);
		$cityCode = $items['City']['attr']['Code'];
		$hotelCode = $cityCode .'_'. $itemCode;
		
		$addressLines = $items['HotelInformation']['AddressLines'];
		$AddressLine1 = isset($addressLines['AddressLine1']['value']) ? addslashes($addressLines['AddressLine1']['value']) : '';
		$AddressLine2 = isset($addressLines['AddressLine2']['value']) ? addslashes($addressLines['AddressLine2']['value']) : '';
		$AddressLine3 = isset($addressLines['AddressLine3']['value']) ? addslashes($addressLines['AddressLine3']['value']) : '';
		$AddressLine4 = isset($addressLines['AddressLine4']['value']) ? addslashes($addressLines['AddressLine4']['value']) : '';
		$Telephone	  = isset($addressLines['Telephone']['value']) ? addslashes($addressLines['Telephone']['value']) : '';
		$Fax		  = isset($addressLines['Fax']['value']) ? addslashes($addressLines['Fax']['value']) : '';
		$EmailAddress = isset($addressLines['EmailAddress']['value']) ? addslashes($addressLines['EmailAddress']['value']) : '';
		$WebSite	  = isset($addressLines['WebSite']['value']) ? addslashes($addressLines['WebSite']['value']) : '';
		
		$starRating   = $items['HotelInformation']['StarRating']['value'];
		$category	  = addslashes($items['HotelInformation']['Category']['value']);
		if (isset($items['HotelInformation']['GeoCodes'])) {
			$latitude	  = $items['HotelInformation']['GeoCodes']['Latitude']['value'];
			$longitude	  = $items['HotelInformation']['GeoCodes']['Longitude']['value'];
		} else {
			$latitude = $longitude = null;
		}
		$roomCount 	  = isset($items['HotelInformation']['RoomTypes']['attr']['RoomCount']) ? $items['HotelInformation']['RoomTypes']['attr']['RoomCount'] : '';
		
		// map zipcode
		$zipcode = '';
		foreach(compact('AddressLine2', 'AddressLine3', 'AddressLine4') as $addrLine) {
			if (!empty($addrLine) && empty($zipcode)) {
				$pieces = explode(" ", $addrLine);
				if (count($pieces) > 0) {
					foreach ($pieces as $piece) {
						if (is_numeric($piece) && strlen($piece) >= 5) {
							$zipcode = $piece;    
						}
					}
				}
			}
		}
		
		// add hotel
		echo "{$itemCode} - {$itemName} \r\n";
		$hotelModel = new Hotel();
		$hotelModel->Code = $hotelCode;
		$hotelModel->ItemCode = $itemCode;
		$hotelModel->ItemName = $itemName;
		$hotelModel->CityCode = $cityCode;
		$hotelModel->Category = $category;
		$hotelModel->StarRating = $starRating;
		$hotelModel->AddressLine1 = $AddressLine1;
		$hotelModel->AddressLine2 = $AddressLine2;
		$hotelModel->AddressLine3 = $AddressLine3;
		$hotelModel->AddressLine4 = $AddressLine4;
		$hotelModel->Telephone = $Telephone;
		$hotelModel->Fax = $Fax;
		$hotelModel->EmailAddress = $EmailAddress;
		$hotelModel->WebSite = $WebSite;
		$hotelModel->Zipcode = $zipcode;
		$hotelModel->Latitude = $latitude;
		$hotelModel->Longitude = $longitude;
		$hotelModel->RoomCount = $roomCount;
		$date = new \DateTime();
		$hotelModel->LastUpdated = $date->format('Y-m-d H:i:s');  
		if ($foundOne = Hotel::findOne($hotelCode)) {
			$hotelModel->Code = $foundOne->Code;
			$hotelModel->update();
		} else {
			$hotelModel->insert();
		}
		
		// add location (locationCode, locationName)
		// add hotel_location (hotelCode, locationCode)
		if (isset($items['LocationDetails']['Location'])) {
			$locations = $items['LocationDetails']['Location'];
			HotelLocation::deleteAll('HotelCode = \''.$hotelCode.'\'');
			if (isset($locations['value'])) {
				$locationCode = $locations['attr']['Code'];
				$locationName = addslashes($locations['value']);
				
				if (Location::findOne(['CityCode' => $cityCode, 'LocationCode' => $locationCode]) == null) {
					$locationModel = new Location();
					$locationModel->CityCode = $cityCode;
					$locationModel->LocationCode = $locationCode;
					$locationModel->LocationName = $locationName;
					$locationModel->insert();
				}
	
				$hotelLocationModel = new HotelLocation();
				$hotelLocationModel->HotelCode = $hotelCode;
				$hotelLocationModel->LocationCode = $locationCode;
				$hotelLocationModel->insert();
			} else {
				foreach($locations as $location) {
					$locationCode = $location['attr']['Code'];
					$locationName = addslashes($location['value']);
	
					if (Location::findOne(['CityCode' => $cityCode, 'LocationCode' => $locationCode]) == null) {
						$locationModel = new Location();
						$locationModel->CityCode = $cityCode;
						$locationModel->LocationCode = $locationCode;
						$locationModel->LocationName = $locationName;
						$locationModel->insert();
					}
	
					if (HotelLocation::findOne(['HotelCode' => $hotelCode, 'LocationCode' => $locationCode]) == null) {
						$hotelLocationModel = new HotelLocation();
						$hotelLocationModel->HotelCode = $hotelCode;
						$hotelLocationModel->LocationCode = $locationCode;
						$hotelLocationModel->insert();
					}
				}
			}
		}
		
		// add hotel_areadetail (hotelCode, areaDetail)
		if (isset($items['HotelInformation']['AreaDetails']['AreaDetail'])) {
			$areaDetails = $items['HotelInformation']['AreaDetails']['AreaDetail'];
			HotelAreaDetail::deleteAll('HotelCode = \''.$hotelCode.'\'');
			if (isset($areaDetails['value'])) {
				$areaDetailValue = addslashes($areaDetails['value']);

				$hotelAreaDetailModel = new HotelAreaDetail();
				$hotelAreaDetailModel->HotelCode = $hotelCode;
				$hotelAreaDetailModel->AreaDetail = $areaDetailValue;
				$hotelAreaDetailModel->insert();
			} else {
				foreach($areaDetails as $areaDetail) {
					$areaDetailValue = addslashes($areaDetail['value']);

					$hotelAreaDetailModel = new HotelAreaDetail();
					$hotelAreaDetailModel->HotelCode = $hotelCode;
					$hotelAreaDetailModel->AreaDetail = $areaDetailValue;
					$hotelAreaDetailModel->insert();
				}
			}
		}
		
		// add report (hotelCode, report, reportType)
		if (isset($items['HotelInformation']['Reports']['Report'])) {
			$reports = $items['HotelInformation']['Reports']['Report'];
			$reportsArray = array();
			Report::deleteAll('HotelCode = \''.$hotelCode.'\'');
			if (isset($reports['value'])) {
				$reportValue = addslashes($reports['value']);
				$reportType = $reports['attr']['Type'];
				
				$reportModel = new Report();
				$reportModel->HotelCode = $hotelCode;
				$reportModel->Report = $reportValue;
				$reportModel->ReportType = $reportType;
				$reportModel->insert();
				
				$reportsArray[$reportType] = $reportValue;
			} else {
				foreach($reports as $report) {
					$reportValue = addslashes($report['value']);
					$reportType = $report['attr']['Type'];
	
					$reportModel = new Report();
					$reportModel->HotelCode = $hotelCode;
					$reportModel->Report = $reportValue;
					$reportModel->ReportType = $reportType;
					$reportModel->insert();
	
					$reportsArray[$reportType] = $reportValue;
				}
			}
			
			$description = json_encode($reportsArray);
		
			// update description
			if ($hotelModel = Hotel::findOne($hotelCode)) {
				$hotelModel->Description = $description;
				$hotelModel->update();
			}
		}

		// add hotel_roomcategory (hotelCode, roomCategoryID, description)
		if (isset($items['HotelInformation']['RoomCategories']['RoomCategory'])) {
			$roomCategories = $items['HotelInformation']['RoomCategories']['RoomCategory'];
			HotelRoomCategory::deleteAll('HotelCode = \''.$hotelCode.'\'');
			if (isset($roomCategories['attr']['Id'])) {
				$roomCategoryID  =  $roomCategories['attr']['Id'];
				$Description =  isset($roomCategories['Description']['value']) ? addslashes($roomCategories['Description']['value']) : '';
				$RoomDescription = isset($roomCategories['RoomDescription']['value']) ? addslashes($roomCategories['RoomDescription']['value']) : '';

				$hotelRoomCategoryModel = new HotelRoomCategory();
				$hotelRoomCategoryModel->HotelCode = $hotelCode;
				$hotelRoomCategoryModel->RoomCategoryID = $roomCategoryID;
				$hotelRoomCategoryModel->Description = $Description;
				$hotelRoomCategoryModel->RoomDescription = $RoomDescription;
				$hotelRoomCategoryModel->insert();
			} else {
				foreach($roomCategories as $roomCategory) {
					$roomCategoryID  =  $roomCategory['attr']['Id'];
					$Description =  isset($roomCategory['Description']['value']) ? addslashes($roomCategory['Description']['value']) : '';
					$RoomDescription = isset($roomCategory['RoomDescription']['value']) ? addslashes($roomCategory['RoomDescription']['value']) : '';
				
					$hotelRoomCategoryModel = new HotelRoomCategory();
					$hotelRoomCategoryModel->HotelCode = $hotelCode;
					$hotelRoomCategoryModel->RoomCategoryID = $roomCategoryID;
					$hotelRoomCategoryModel->Description = $Description;
					$hotelRoomCategoryModel->RoomDescription = $RoomDescription;
					$hotelRoomCategoryModel->insert();
				}
			}
		}
		
		// add hotel_roomtype (hotelCode, roomTypeCode)
		if (isset($items['HotelInformation']['RoomTypes']['RoomType'])) {
			$roomTypes = $items['HotelInformation']['RoomTypes']['RoomType'];
			HotelRoomType::deleteAll('ItemCode = \''.$hotelCode.'\'');
			if (isset($roomTypes['value'])) {
				$roomTypeName = addslashes($roomTypes['value']);
				$roomTypeCode = $roomTypes['attr']['Code'];
				
				if (HotelRoomType::findOne(['ItemCode' => $hotelCode, 'RoomTypeCode' => $roomTypeCode]) == null) {
					$hotelRoomTypeModel = new HotelRoomType();
					$hotelRoomTypeModel->ItemCode = $hotelCode;
					$hotelRoomTypeModel->RoomTypeCode = $roomTypeCode;
					$hotelRoomTypeModel->insert();
				}
			} else {
				foreach($roomTypes as $roomType) {
					$roomTypeName = addslashes($roomType['value']);
					$roomTypeCode = $roomType['attr']['Code'];
					
					if (HotelRoomType::findOne(['ItemCode' => $hotelCode, 'RoomTypeCode' => $roomTypeCode]) == null) {
						$hotelRoomTypeModel = new HotelRoomType();
						$hotelRoomTypeModel->ItemCode = $hotelCode;
						$hotelRoomTypeModel->RoomTypeCode = $roomTypeCode;
						$hotelRoomTypeModel->insert();
					}
				}
			}
		}
		
		// add roomfacility (Code, roomFacility)
		// add hotel_roomfacility (hotelCode, roomFacilityCode)
		if (isset($items['HotelInformation']['RoomFacilities']['Facility'])) {
			$roomFacilities = $items['HotelInformation']['RoomFacilities']['Facility'];
			HotelRoomFacility::deleteAll('HotelCode = \''.$hotelCode.'\'');
			if (isset($roomFacilities['value'])) {
				$roomFacilities[0] = $roomFacilities;
				unset($roomFacilities['value']);
				unset($roomFacilities['attr']);
			}
			foreach($roomFacilities as $roomFacility) {
				$roomFacilityValue = addslashes($roomFacility['value']);
				$roomFacilityCode = $roomFacility['attr']['Code'];

				if (RoomFacility::findOne($roomFacilityCode) == null) {
					$roomFacilityModel = new RoomFacility();
					$roomFacilityModel->Code = $roomFacilityCode;
					$roomFacilityModel->RoomFacility = $roomFacilityValue;
					$roomFacilityModel->insert();
				}

				$hotelRoomFacilityModel = new HotelRoomFacility();
				$hotelRoomFacilityModel->HotelCode = $hotelCode;
				$hotelRoomFacilityModel->RoomFacilityCode = $roomFacilityCode;
				$hotelRoomFacilityModel->insert();
			}
		}
		
		// add facility (Code, Facility)
		// add hotel_facility (hotelCode, FacilityCode, Facility)
		if (isset($items['HotelInformation']['Facilities']['Facility'])) {
			$facilities = $items['HotelInformation']['Facilities']['Facility'];
			HotelFacility::deleteAll('HotelCode = \''.$hotelCode.'\'');
			if (isset($facilities['value'])) {
				$facilities[0] = $facilities;
				unset($facilities['value']);
				unset($facilities['attr']);
			}
			foreach($facilities as $facility) {
				$facilityValue = addslashes($facility['value']);
				$facilityCode = $facility['attr']['Code'];
				if (Facility::findOne($facilityCode) == null) {
					$facilityModel = new Facility();
					$facilityModel->Code = $facilityCode;
					$facilityModel->Facility = $facilityValue;
					$facilityModel->insert();
				}

				$hotelFacilityModel = new HotelFacility();
				$hotelFacilityModel->HotelCode = $hotelCode;
				$hotelFacilityModel->FacilityCode = $facilityCode;
				$hotelFacilityModel->Facility = $facilityValue;
				$hotelFacilityModel->insert();
			}
		}

		// add hotel_linkmap (itemCode, mapLink)
		if (isset($items['HotelInformation']['Links'])) {
			if (isset($items['HotelInformation']['Links']['MapLinks'])) {
				$mapLinks = $items['HotelInformation']['Links']['MapLinks'];
				$mapLink = $mapLinks['MapPageLink']['value'];
	
				HotelLinkmap::deleteAll('ItemCode = \''.$hotelCode.'\'');
				$hotelLinkmapModel = new HotelLinkmap();
				$hotelLinkmapModel->ItemCode = $hotelCode;
				$hotelLinkmapModel->MapLink = $mapLink;
				$hotelLinkmapModel->insert();
			}

			if (isset($items['HotelInformation']['Links']['ImageLinks'])) {
				// add hotel_image (hotelCode, imageText, imageHeight, imageWidth, imageUrl, imageUrlThumbnail)
				$imageLinks = $items['HotelInformation']['Links']['ImageLinks']['ImageLink'];
				HotelImage::deleteAll('HotelCode = \''.$hotelCode.'\'');
				if (isset($imageLinks['Image']['value'])) {
					if (isset($imageLinks['Text'])) {
						$imageText = addslashes($imageLinks['Text']['value']);
					} else {
						$imageText = null;
					}
					$imageUrl = $imageLinks['Image']['value'];
					$imageUrlThumbnail = $imageLinks['ThumbNail']['value'];
					$imageHeight = $imageLinks['attr']['Height'];
					$imageWidth = $imageLinks['attr']['Width'];
		
					$hotelImageModel = new HotelImage();
					$hotelImageModel->HotelCode = $hotelCode;
					$hotelImageModel->ImageText = $imageText;
					$hotelImageModel->ImageHeight = $imageHeight;
					$hotelImageModel->ImageWidth = $imageWidth;
					$hotelImageModel->ImageUrl = $imageUrl;
					$hotelImageModel->ImageUrlThumbnail = $imageUrlThumbnail;
					$hotelImageModel->insert();
				} else {
					foreach($imageLinks as $imageLink) {
						if (isset($imageLinks['Text'])) {
							$imageText = addslashes($imageLink['Text']['value']);
						} else {
							$imageText = null;
						}
						$imageUrl = $imageLink['Image']['value'];
						$imageUrlThumbnail = $imageLink['ThumbNail']['value'];
						$imageHeight = $imageLink['attr']['Height'];
						$imageWidth = $imageLink['attr']['Width'];
						
						$hotelImageModel = new HotelImage();
						$hotelImageModel->HotelCode = $hotelCode;
						$hotelImageModel->ImageText = $imageText;
						$hotelImageModel->ImageHeight = $imageHeight;
						$hotelImageModel->ImageWidth = $imageWidth;
						$hotelImageModel->ImageUrl = $imageUrl;
						$hotelImageModel->ImageUrlThumbnail = $imageUrlThumbnail;
						$hotelImageModel->insert();
					}
				}
			}
		}		
	}
    
    private static function deleteDir($path)
    {
    	$class_func = array(__CLASS__, __FUNCTION__);
    	return is_file($path) ?
    	@unlink($path) :
    	array_map($class_func, glob($path.'/*')) == @rmdir($path);
    }
}
