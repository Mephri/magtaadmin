<?php

namespace app\controllers;

use Yii;
use app\models\gta\Country;
use app\models\gta\Area;
use app\models\gta\City;
use app\models\gta\Location;
use app\models\gta\Hotel;
use app\models\gta\HotelFacility;
use app\models\gta\RoomType;
use app\models\ma\Hotel as MAHotel;
use app\models\ma\LocationCountry as MALocationCountry;
use app\models\ma\LocationState as MALocationState;
use app\models\ma\LocationCity as MALocationCity;
use app\models\ma\LocationArea as MALocationArea;
use app\models\ma\TranslationText as MATranslationText;
use app\models\ma\HotelFacility as MAHotelFacility;
use app\models\ma\HotelRoomFacility as MAHotelRoomFacility;
use app\models\ma\JoinHotelFacility as MAJoinHotelFacility;
use app\models\ma\HotelPhoto as MAHotelPhoto;
use app\models\ma\HotelRoom as MAHotelRoom;
use app\models\ma\GtaHotelRoomType as MAGtaHotelRoomType;
use app\models\ma\GtaHotelRoomCategory as MAGtaHotelRoomCategory;
use yii\web\BadRequestHttpException;

class GtaSyncController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->actionViewCountries();
    }

	public function actionViewCountries() {
		$countries = Country::find()->select('country.*')->all();
		return $this->render('view-countries', ['countries' => $countries]);
	}
	
	public function actionViewAreas() {
		$countryCode = Yii::$app->request->post('countryCode', null);
		if ($countryCode) {
			$areas = Area::find()
				->select('area.*')
				->innerJoin('country', 'country.Code = area.CountryCode')
				->where(['country.Code' => $countryCode])
				->all();
			$country = Country::findOne($countryCode);
			return $this->render('view-areas', ['areas' => $areas, 'country' => $country]);
		} else {
			throw new BadRequestHttpException();
		}
	}
	
	public function actionViewCities() {
		$countryCode = Yii::$app->request->post('countryCode', null);
		$areaCode = Yii::$app->request->post('areaCode', null);
		if ($countryCode) {
			$query = City::find()
				->select('city.*')
				->innerJoin('area', 'area.Code = city.AreaCode')
				->innerJoin('country', 'country.Code = area.CountryCode')
				->where(['country.Code' => $countryCode]);
			if ($areaCode) {
				$query->andWhere(['area.Code' => $areaCode]);
			}
			$cities = $query->all();
			$country = Country::findOne($countryCode);
			$area = Area::findOne($areaCode);
			return $this->render('view-cities', ['cities' => $cities, 'area' => $area, 'country' => $country]);
		} else {
			throw new BadRequestHttpException();
		}
	}
	
	public function actionViewLocations() {
		$countryCode = Yii::$app->request->post('countryCode');
		$cityCode = Yii::$app->request->post('cityCode');
		if ($countryCode && $cityCode) {
			$locations = Location::find()
				->select('location.*')->distinct()
				->innerJoin('hotel_location', 'hotel_location.LocationCode = location.LocationCode')
				->innerJoin('city', 'city.Code = location.CityCode')
				->innerJoin('area', 'area.Code = city.AreaCode')
				->innerJoin('country', 'country.Code = area.CountryCode')
				->where(['country.Code' => $countryCode])
				->andWhere(['city.Code' => $cityCode])
				->all();
			$country = Country::findOne($countryCode);
			$city = City::findOne($cityCode);
			return $this->render('view-locations', ['locations' => $locations, 'city' => $city, 'country' => $country]);
		} else {
			throw new BadRequestHttpException();
		}
	}
	
	public function actionViewHotels() {
		$countryCode = Yii::$app->request->post('countryCode');
		$cityCode = Yii::$app->request->post('cityCode');
		$locationCode = Yii::$app->request->post('locationCode');
		if ($countryCode && $cityCode && $locationCode) {
			$hotels = Hotel::find()
				->select('hotel.*')
				->innerJoin('hotel_location', 'hotel_location.HotelCode = hotel.Code')
				->innerJoin('location', ['and', 'location.CityCode = hotel.CityCode', 'location.LocationCode = hotel_location.LocationCode']) 
				->innerJoin('city', 'city.Code = location.CityCode')
				->innerJoin('area', 'area.Code = city.AreaCode')
				->innerJoin('country', 'country.Code = area.CountryCode')
				->where(['country.Code' => $countryCode])
				->andWhere(['city.Code' => $cityCode])
				->andWhere(['location.LocationCode' => $locationCode])
				->all();
			$country = Country::findOne($countryCode);
			$city = City::findOne($cityCode);
			$location = Location::findOne(['CityCode' => $cityCode, 'LocationCode' => $locationCode]);
			return $this->render('view-hotels', ['hotels' => $hotels, 'location' => $location, 'city' => $city, 'country' => $country]);
		} else {
			throw new BadRequestHttpException();
		}
	}
	
	public function actionViewHotelsInCountry() {
		$countryCode = Yii::$app->request->post('countryCode');
		if ($countryCode) {
			$hotels = $this->getHotels($countryCode);
			$country = Country::findOne($countryCode);
			return $this->render('view-hotels-in-country', ['hotels' => $hotels, 'country' => $country]);
		} else {
			throw new BadRequestHttpException();
		}
	}
	
	public function actionViewHotelsInArea() {
		$countryCode = Yii::$app->request->post('countryCode');
		$areaCode = Yii::$app->request->post('areaCode');
		if ($countryCode && $areaCode) {
			$hotels = $this->getHotels($countryCode, $areaCode);
			$country = Country::findOne($countryCode);
			$area = Area::findOne($areaCode);
			return $this->render('view-hotels-in-area', ['hotels' => $hotels, 'area' => $area, 'country' => $country]);
		} else {
			throw new BadRequestHttpException();
		}
	}
	
	public function actionViewHotelsInAreaCity() {
		$countryCode = Yii::$app->request->post('countryCode');
		$areaCode = Yii::$app->request->post('areaCode');
		$cityCode = Yii::$app->request->post('cityCode');
		if ($countryCode && $areaCode && $cityCode) {
			$hotels = $this->getHotels($countryCode, $areaCode, $cityCode);
			$country = Country::findOne($countryCode);
			$area = Area::findOne($areaCode);
			$city = City::findOne($cityCode);
			return $this->render('view-hotels-in-area-city', ['hotels' => $hotels, 'city' => $city, 'area' => $area, 'country' => $country]);
		} else {
			throw new BadRequestHttpException();
		}
	}
	
	public function actionViewHotelsInCity() {
		$countryCode = Yii::$app->request->post('countryCode');
		$cityCode = Yii::$app->request->post('cityCode');
		if ($countryCode && $cityCode) {
			$hotels = $this->getHotels($countryCode, null, $cityCode);
			$country = Country::findOne($countryCode);
			$city = City::findOne($cityCode);
			return $this->render('view-hotels-in-city', ['hotels' => $hotels, 'city' => $city, 'country' => $country]);
		} else {
			throw new BadRequestHttpException();
		}
	}
	
	public function actionViewHotelsInLocation() {
		$countryCode = Yii::$app->request->post('countryCode');
		$cityCode = Yii::$app->request->post('cityCode');
		$locationCode = Yii::$app->request->post('locationCode');
		if ($countryCode && $cityCode && $locationCode) {
			$hotels = $this->getHotels($countryCode, null, $cityCode, $locationCode);
			$country = Country::findOne($countryCode);
			$city = City::findOne($cityCode);
			$location = Location::findOne(['CityCode' => $cityCode, 'LocationCode' => $locationCode]);
			return $this->render('view-hotels-in-location', ['hotels' => $hotels, 'location' => $location, 'city' => $city, 'country' => $country]);
		} else {
			throw new BadRequestHttpException();
		}
	}
	
	public function actionMapSyncCountries() {
		$countries = Yii::$app->request->post('countries');
		foreach ($countries as $country) {
			if ($country['selected']) {
				$hotels = $this->getHotels($country->Code);
				$this->insertOrUpdateHotels($country['enabled'], $hotels);
			}
		}
		return $this->redirect(['gta-sync/view-countries']);
    }
    
    public function actionMapSyncAreas() {
    	$areas = Yii::$app->request->post('areas');
    	$countryCode = Yii::$app->request->post('countryCode');
    	foreach ($areas as $area) {
    		if ($area['selected']) {
	    		$hotels = $this->getHotels($country->Code, $area->Code);
	    		$this->insertOrUpdateHotels($area['enabled'], $hotels);
    		}
    	}
		return $this->redirect(['gta-sync/view-areas', 'countryCode' => $countryCode]);
    }
    
    public function actionMapSyncCities() {
    	$cities = Yii::$app->request->post('cities');
    	$countryCode = Yii::$app->request->post('countryCode');
    	$areaCode = Yii::$app->request->post('areaCode');
    	foreach ($cities as $city) {
    		if ($city['selected']) {
	    		$hotels = $this->getHotels($country->Code, $area->Code, $city->Code);
	    		$this->insertOrUpdateHotels($city['enabled'], $hotels);
    		}
    	}
    	return $this->redirect(['gta-sync/view-cities', 'countryCode' => $countryCode, 'areaCode' => $areCode]);
    }
    
    public function actionMapSyncLocations() {
    	$locations = Yii::$app->request->post('locations');
    	$countryCode = Yii::$app->request->post('countryCode');
    	$cityCode = Yii::$app->request->post('cityCode');
    	foreach ($locations as $location) {
    		if ($location['selected']) {
	    		$hotels = $this->getHotels($country->Code, $area->Code, $city->Code, $location->Code);
	    		$this->insertOrUpdateHotels($location['enabled'], $hotels);
    		}
    	}
    	return $this->redirect(['gta-sync/view-locations', 'countryCode' => $countryCode, 'cityCode' => $cityCode]);
    }
    
    public function actionMapSyncHotels() {
    	$hotels = Yii::$app->request->post('hotels');
    	$countryCode = Yii::$app->request->post('countryCode');
    	$cityCode = Yii::$app->request->post('cityCode');
    	$locationCode =Yii::$app->request->post('locationCode');
    	$this->insertOrUpdateHotels(null, $hotels);
    	return $this->redirect(['gta-sync/view-hotels', 'countryCode' => $countryCode, 'cityCode' => $cityCode, 'locationCode' => $locationCode]);
    }
    
    private function getHotels($countryCode = 'ID', $areaCode = null, $cityCode = null, $locationCode = null) {
    	$query = Hotel::find()
    		->select('hotel.*')
    		->innerJoin('hotel_location', 'hotel_location.HotelCode = hotel.Code')
    		->innerJoin('location', ['and', 'location.CityCode = hotel.CityCode', 'location.LocationCode = hotel_location.LocationCode'])
    		->innerJoin('city', 'city.Code = location.CityCode')
    		->innerJoin('area', 'area.Code = city.AreaCode')
    		->innerJoin('country', 'country.Code = area.CountryCode')
    		->where(['country.Code' => $countryCode]);
    	if ($areaCode) {
    		$query->andWhere(['area.Code' => $areaCode]);
    	}
    	if ($cityCode) {
    		$query->andWhere(['city.Code' => $cityCode]);
    	}
    	if ($locationCode) {
    		$query->andWhere(['location.LocationCode' => $locationCode]);
    	}
    	return $query->all();
    }

    private function insertOrUpdateHotels($enabled = null, $hotels = array()) {
    	foreach ($hotels as $hotel) {
    		if ($hotel['LastUpdated'] && $hotel['LastSynced']) {	// possibly do update
	    		$lastSynced = new \DateTime($hotel['LastSynced']);
	    		$lastUpdated = new \DateTime($hotel['LastUpdated']);
	    		if ($lastSynced > $lastUpdated) {	// do update
	    			$maHotelModel = $this->syncHotels($hotel, MAHotel::findOne(['hotel_code' => $hotel->Code]), true);
					$this->syncHotelFacilities($hotel, $maHotelModel);
					$this->syncHotelImages($hotel, $maHotelModel);
					$this->syncRoomTypes($hotel, $maHotelModel);
					$this->syncHotelRoomCategories($hotel, $maHotelModel);
					$this->syncHotelRoomFacilities($hotel);
	    		}
    		} else {	// do insert
    			$maHotelModel = $this->syncHotels($hotel, false);
				$this->syncHotelFacilities($hotel, $maHotelModel);
				$this->syncHotelImages($hotel, $maHotelModel);
				$this->syncRoomTypes($hotel, $maHotelModel);
				$this->syncHotelRoomCategories($hotel, $maHotelModel);
				$this->syncHotelRoomFacilities($hotel);
    		}
    	}
    }
    
    private function getMAArea($hotel) {
    	$city = City::find()
	    	->select('city.CityName as CityName, country.Code as CountryCode, country.CountryName as CountryName, area.AreaName as AreaName')
	    	->innerJoin('country', 'country.Code = city.CountryCode')
	    	->leftJoin('area', 'area.Code = city.AreaCode')
	    	->where(['city.Code' => $hotel['CityCode']])
	    	->one();
    	$maCountry = MALocationCountry::find()
	    	->select('location_country.id as id')
	    	->where(['code' => $city->CountryCode, 'name' => $city->CountryName])
	    	->one();
    	if ($city->AreaName == null) {
    		$areaName = 'Unspecified';
    	} else {
    		$areaName = $city->AreaName;
    	}
    	$maState = MALocationState::find()
	    	->select('location_state.id as id')
	    	->where(['country_id' => $maCountry->id, 'name' => $areaName])
	    	->one();
    	$maCity = MALocationCity::find()
	    	->select('location_city.id as id')
	    	->where(['state_id' => $maState->id, 'name' => $city->CityName])
	    	->one();
    	$location = Location::findOne(['HotelCode' => $hotel['Code']]);
    	$maArea = MALocationArea::find()
	    	->select('location_area.id as id')
	    	->where(['city_id' => $maCity->id, 'name' => $location->LocationName])
	    	->one();
    	return $maArea;
    }
    
    private function syncHotels($hotel, $maHotel, $doUpdate) {
    	$maArea = $this->getMAArea($hotel);
    	if ($maHotel == null) {
    		$maHotelModel = new MAHotel();
    	} else {
    		$maHotelModel = $maHotel;
    	}
    	$maHotelModel->name = $hotel['ItemName'];
    	$city = City::findOne(['Code' => $hotel['CityCode']]);
    	$maHotelModel->area_id = $maArea->id;
    	$maHotelModel->star_rating = $hotel['StarRating'];
    	$maHotelModel->number_room = $hotel['RoomCount'];
    	$maHotelModel->address = $hotel['AddressLine1'] . ' '
    			. $hotel['AddressLine2'] . ' '
    			. $hotel['AddressLine3'] . ' '
    			. $hotel['AddressLine4'];
    	$maHotelModel->phone = $hotel['Telephone'];
    	$maHotelModel->fax = $hotel['Fax'];
    	$maHotelModel->website = $hotel['WebSite'];
    	$maHotelModel->zipcode = $hotel['ZipCode'];
    	$maHotelModel->latitude = $hotel['Latitude'];
    	$maHotelModel->longitude = $hotel['Longitude'];
    	if ($doUpdate) {
    		$maTranslationTextModel = MATranslationText::findOne(['translate_id' => $hotel->desc_trans_id]);
    		$maTranslationTextModel->translate_type = 'description';
    		$maTranslationTextModel->output_en = $hotel['Description'];
    		$maTranslationTextModel->update();
    	} else {
    		$maTranslationTextModel = new MATranslationText();
    		$maTranslationTextModel->translate_type = 'description';
    		$maTranslationTextModel->output_en = $hotel['Description'];
    		$maTranslationTextModel->insert();
    	}
    	$maHotelModel->desc_trans_id = $maTranslationTextModel->translate_id;
    	if ($doUpdate) {
    		$maHotelModel->update();
    	} else {
    		$maHotelModel->insert();
    	}
    	$hotelModel = Hotel::findOne($hotel['Code']);
    	$date = new \DateTime();
    	$hotelModel->LastSynced = $date->format('Y-m-d H:i:s');
    	$hotelModel->update();
    	return $maHotelModel;
    }
    
    private function syncHotelFacilities($hotel, $maHotelModel) {
    	$hotelFacilities = HotelFacility::find()
	    	->where(['HotelCode' => $hotel['Code']])
	    	->all();
    	foreach ($hotelFacilities as $hotelFacility) {
    		$maHotelFacility = MAHotelFacility::find()
	    		->where(['facility_name' => $hotelFacility->Facility])
	    		->one();
    		if (JoinHotelFacility::findOne(['hotel_id' => $maHotelModel->hotel_id, 'facility_id' => $maHotelFacility->facility_id]) == null) {
    			$maJoinHotelFacilityModel = new MAJoinHotelFacility();
    			$maJoinHotelFacilityModel->hotel_id = $maHotelModel->hotel_id;
    			$maJoinHotelFacilityModel->facility_id = $maHotelFacility->facility_id;
    			$maJoinHotelFacilityModel->insert();
	    		$date = new \DateTime();
	    		$hotelFacility->LastSynced = $date->format('Y-m-d H:i:s');
	    		$hotelFacility->update();
    		}
    	}
    }
    
    private function syncHotelImages($hotel, $maHotelModel) {
    	$hotelImages = HotelImage::find()
	    	->where(['HotelCode' => $hotel['Code']])
	    	->all();
    	MAHotelPhoto::deleteAll('hotel_id = ' . $maHotelModel->hotel_id);
    	foreach ($hotelImages as $hotelImage) {
    		$maHotelPhotoModel = new MAHotelPhoto();
    		$maHotelPhotoModel->hotel_id = $maHotelModel->hotel_id;
    		$maHotelPhotoModel->filename = $hotelImage->ImageUrl;
    		$maHotelPhotoModel->photo_type_id = 1;
    		$maHotelPhotoModel->primary = 1;
    		$maHotelPhotoModel->status = 'active';
    		$maHotelPhotoModel->insert();
    		$date = new \DateTime();
    		$hotelImage->LastSynced = $date->format('Y-m-d H:i:s');
    		$hotelImage->update();
    	}
    }
    
    private function syncRoomTypes($hotel, $maHotelModel) {
    	$roomTypes = RoomType::find()
	    	->select('roomtype.*')
	    	->innerJoin('hotel_roomtype', 'roomtype.Code = hotel_roomtype.RoomTypeCode')
	    	->where(['hotel_roomtype.ItemCode' => $hotel['Code']])
	    	->all();
    	MAGtaHotelRoomType::deleteAll('hotel_id = ' . $maHotelModel->hotel_id);
    	foreach ($roomTypes as $roomType) {
    		$maGtaHotelRoomTypeModel = new MAGtaHotelRoomType();
    		$maGtaHotelRoomTypeModel->code = $roomType->Code;
    		$maGtaHotelRoomTypeModel->name = $roomType->RoomTypeName;
    		$maGtaHotelRoomTypeModel->hotel_id = $maHotelModel->hotel_id;
    		$maGtaHotelRoomTypeModel->insert();
    	}
    }
    
    private function syncHotelRoomCategories($hotel, $maHotelModel) {
    	$hotelRoomCategories = HotelRoomCategory::find()
	    	->where(['HotelCode' => $hotel['Code']])
	    	->all();
    	MAHotelRoom::deleteAll('hotel_id = ' . $maHotelModel->hotel_id);
    	MAGtaHotelRoomCategory::deleteAll('hotel_id = ' . $maHotelModel->hotel_id);
    	foreach ($hotelRoomCategories as $hotelRoomCategory) {
    		$maHotelRoomTypeModel = MAHotelRoomType::findOne(['room_type_name' => $hotelRoomCategory->Description]);
    		if ($maHotelRoomTypeModel == null) {
    			$maHotelRoomTypeModel = new MAHotelRoomType();
    			$maHotelRoomTypeModel->room_type_name = $hotelRoomCategory->Description;
    			$maHotelRoomTypeModel->insert();
    		}
    		$maHotelRoomModel = new MAHotelRoom();
    		$maHotelRoomModel->hotel_id = $maHotelModel->hotel_id;
    		$maHotelRoomModel->room_type_id = $maHotelRoomTypeModel->room_type_id;
    		$maHotelRoomModel->room_name = $hotelRoomCategory->Description;
    		$maHotelRoomModel->insert();
    	
    		$maGtaHotelRoomCategoryModel = new MAGtaHotelRoomCategory();
    		$maGtaHotelRoomCategoryModel->category_id = $hotelRoomCategory->RoomCategoryID;
    		$maGtaHotelRoomCategoryModel->hotel_id = $maHotelModel->hotel_id;
    		$maGtaHotelRoomCategoryModel->room_type_id = $maHotelRoomTypeModel->room_type_id;
    		$maGtaHotelRoomCategoryModel->insert();
    	
    		$date = new \DateTime();
    		$hotelRoomCategory->LastSynced = $date->format('Y-m-d H:i:s');
    		$hotelRoomCategory->update();
    	}
    }
    
    private function syncHotelRoomFacilities($hotel) {
    	$hotelRoomFacilities = HotelRoomFacility::find()
	    	->select('hotel_roomfacility.*, roomfacility.RoomFacility as RoomFacilityName')
	    	->innerJoin('roomfacility', 'roomfacility.Code = hotel_roomfacility.RoomFacilityCode')
	    	->where(['hotel_roomfacility.HotelCode' => $hotel['Code']])
	    	->all();
    	foreach ($hotelRoomFacilities as $hotelRoomFacility) {
    		$maHotelRoomFacility = MAHotelRoomFacility::find()
	    		->where(['room_facility_name' => $hotelRoomFacility->RoomFacilityName])
	    		->one();
    		if (JoinHotelRoomFacility::findOne(['room_id' => $maHotelRoomModel->room_id, 'room_facility_id' => $maHotelRoomFacility->room_facility_id]) == null) {
    			$maJoinHotelRoomFacilityModel = new MAJoinHotelRoomFacility();
    			$maJoinHotelRoomFacilityModel->room_id = $maHotelRoomModel->room_id;
    			$maJoinHotelRoomFacilityModel->room_facility_id = $maHotelRoomFacility->room_facility_id;
    			$maJoinHotelRoomFacilityModel->insert();
	    		$date = new \DateTime();
	    		$hotelRoomFacility->LastSynced = $date->format('Y-m-d H:i:s');
	    		$hotelRoomFacility->update();
    		}
    	}
    }
}
